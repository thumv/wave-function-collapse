﻿using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.U2D;
using System.Web;
public class TileEdgeDetector : MonoBehaviour
{
    public int sampleSize = 5;
    public Tile[] tiles;
    public List<Sprite> tilesInput;
    public SpriteAtlas spriteAtlas;

    public Image tileEdgesPreview;


    internal TileColorSample DetectEdgeColors(Tile importTile)
    {

        Sprite tmpSprite = importTile.sprite;

        //rect inherits the texture position in the sprite atlas/source
        Rect rect = tmpSprite.rect;

        float width = rect.width;
        float height = rect.height;

        Texture2D tex = tmpSprite.texture;


        bool isMipMapLvl = false;

        if (tex.mipmapCount > 1)
            isMipMapLvl = true;

        Texture2D texCopy = new Texture2D(tex.width, tex.height, tex.format, isMipMapLvl);

        Graphics.CopyTexture(tex, texCopy);

        int x_half = (int)(width / 2);
        int y_half = (int)(height / 2);

        //pixel coordinates:
        //y -> from bottom to top
        //x -> from left to right

        Color[] topColours = new Color[sampleSize];
        Color[] bottomColours = new Color[sampleSize];
        Color[] leftColours = new Color[sampleSize];
        Color[] righColours = new Color[sampleSize];

        int x_step = (int)(width / sampleSize);
        int y_step = (int)(height / sampleSize);

        for (int i = 0; i  < sampleSize ; i++)
        {
            float bottom = sampleSize / 4;
            float top = sampleSize * 3 / 4;
            
            float x = Mathf.InverseLerp(bottom, top, rect.x + x_step * (i + 1));
            float y = Mathf.InverseLerp(bottom, top, y_step * (i + 1) + rect.y);

            int margin = 1;

            int x_temp = (int)(rect.x + x_step * (i + 1) + margin - 1);
            int y_temp = (int)(rect.y + margin);

            Color tmpBottom = tex.GetPixel(x_temp, y_temp);
                          texCopy.SetPixel(x_temp, y_temp, Color.red);

            x_temp = (int)(rect.x + x_step * (i + 1) + margin - 1);
            y_temp = (int)(height - margin - 1 + rect.y);

            Color tmpTop = tex.GetPixel(x_temp, y_temp);
                       texCopy.SetPixel(x_temp, y_temp, Color.red);

            x_temp = (int)(rect.x + margin);
            y_temp = (int)(y_step * (i + 1) + rect.y) + margin - 1;

            Color tmpLeft = tex.GetPixel(x_temp, y_temp);
                        texCopy.SetPixel(x_temp, y_temp, Color.red);

            x_temp = (int)(width - margin - 1 + rect.x);
            y_temp = (int)(y_step * (i + 1) + rect.y) + margin - 1;

            Color tmpRight = tex.GetPixel(x_temp, y_temp);
                         texCopy.SetPixel(x_temp, y_temp, Color.red);

            try
            {
                string text = UnityEngine.ColorUtility.ToHtmlStringRGB(tmpTop);

            }
            catch (System.Exception)
            {

                throw;
            }

            margin = 3;
            bottomColours[i] = tex.GetPixel((int)(rect.x + x_step * (i + 1)) + margin - 1, (int)(rect.y + margin));
                           texCopy.SetPixel((int)(rect.x + x_step * (i + 1) + margin - 1), (int)(rect.y + margin), Color.red);

            topColours[i] = tex.GetPixel((int)(rect.x + x_step * (i + 1) + margin - 1), (int)(height - margin - 1 + rect.y));
                        texCopy.SetPixel((int)(rect.x + x_step * (i + 1) + margin - 1), (int)(height - margin - 1 + rect.y), Color.red);

            leftColours[i] = tex.GetPixel((int)(rect.x + margin), (int)(y_step * (i + 1) + rect.y) + margin - 1);
                         texCopy.SetPixel((int)(rect.x + margin), (int)(y_step * (i + 1) + rect.y) + margin - 1, Color.red);

            righColours[i] = tex.GetPixel((int)(width - margin - 1 + rect.x), (int)(y_step * (i + 1) + rect.y) + margin - 1);
                         texCopy.SetPixel((int)(width - margin - 1 + rect.x), (int)(y_step * (i + 1) + rect.y + margin - 1), Color.red);

            //crossing between 2 Samples (0 and 1 margin):
            bottomColours[i].a = (tmpBottom.a + bottomColours[i].a) / 2;
            bottomColours[i].r = (tmpBottom.r + bottomColours[i].r) / 2;
            bottomColours[i].g = (tmpBottom.g + bottomColours[i].g) / 2;
            bottomColours[i].b = (tmpBottom.b + bottomColours[i].b) / 2;

            topColours[i].a = (tmpTop.a + topColours[i].a) / 2;
            topColours[i].r = (tmpTop.r + topColours[i].r) / 2;
            topColours[i].g = (tmpTop.g + topColours[i].g) / 2;
            topColours[i].b = (tmpTop.b + topColours[i].b) / 2;

            leftColours[i].a = (tmpLeft.a + leftColours[i].a) / 2;
            leftColours[i].r = (tmpLeft.r + leftColours[i].r) / 2;
            leftColours[i].g = (tmpLeft.g + leftColours[i].g) / 2;
            leftColours[i].b = (tmpLeft.b + leftColours[i].b) / 2;

            righColours[i].a = (tmpRight.a + righColours[i].a) / 2;
            righColours[i].r = (tmpRight.r + righColours[i].r) / 2;
            righColours[i].g = (tmpRight.g + righColours[i].g) / 2;
            righColours[i].b = (tmpRight.b + righColours[i].b) / 2;

        }


        texCopy.Apply();

        tileEdgesPreview.sprite = Sprite.Create(texCopy, rect, new Vector2(0,0));

        return new TileColorSample(topColours, bottomColours, leftColours, righColours);
    }

    public TileColorSample[] DetectEdgeColors(TileBase[] tiles)
    {
        List<TileColorSample> sampleList = new List<TileColorSample>();

        for (int i = 0; i < tiles.Length; i++)
        {
            Sprite tmpSprite = ((Tile)tiles[i])?.sprite;
            if (tmpSprite != null)
                sampleList.Add(DetectEdgeColors(((Tile)tiles[i])));
            else
                print("tile sample sprite is null at " + i);
        }

        TileColorSample[] samples = sampleList.ToArray();

        return samples;

    }
}
