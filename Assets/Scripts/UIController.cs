﻿using UnityEngine.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.Tilemaps;


public class UIController : MonoBehaviour
{
    public GameObject uiBar;
    public Text  messageText;
    //public GameObject tilePaletteDisplay;
    public Transform tilePaletteDisplayPos;
    public Image importImagePreview;
    public Button toggleImagePreviewBtn;
    private bool uiToggle;
    private readonly float waitTime = 6.0f;
    public Image[] tilePalette;

    void Start()
    {
        uiToggle = true;
        uiBar.SetActive(true);
        messageText.text = "";
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape)){
            uiToggle = !uiToggle;
            uiBar.SetActive(uiToggle);
        }
    }


    public void DisplayMessage(string txt)
    {     
        StartCoroutine(WaitAndDisplayMessage(waitTime, txt));
    }

    private IEnumerator WaitAndDisplayMessage(float waitTime, string txt)
    {
        messageText.text = txt;
        yield return new WaitForSeconds(waitTime);
        messageText.text = "";
    }

    internal void SetTilePaletteUI(TileBase[] tiles)
    {

         for (int i = 0; i < tilePalette.Length; i++)
        {
            tilePalette[i].sprite = default;    
        }

        for (int i = 0; i < tiles.Length; i++)
        {
            Vector3Int tilePosition2 = new Vector3Int(i, 0, 0);
            ITilemap iTileMap = null;
            TileData tileData2 = new TileData();

            tiles[i]?.GetTileData(tilePosition2,iTileMap, ref tileData2);

            if (tilePalette.Length <= i) continue;
            tilePalette[i].sprite = tileData2.sprite;
        }
    }

    internal void SetImportImagePreview(Sprite tmpSprite)
    {
        if(tmpSprite != null)
            importImagePreview.sprite = tmpSprite;
    }


    public void ToggleImagePreview()
    {
        importImagePreview.transform.parent.parent.gameObject.SetActive(!importImagePreview.transform.parent.parent.gameObject.activeSelf);

        if (importImagePreview.IsActive())
            toggleImagePreviewBtn.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Hide Preview";
        else
            toggleImagePreviewBtn.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Show Preview";
    }
}
