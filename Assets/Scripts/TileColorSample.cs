﻿using System.Text;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TileColorSample : MonoBehaviour
{
    public float thresholdValue;
    private Color[] m_topColours;
    private Color[] m_bottomColours;
    private Color[] m_leftColours;
    private Color[] m_righColours;

    // Start is called before the first frame update
    void Start()
    {
        thresholdValue = GameManager.instance.thresholdValue;
    }

    public TileColorSample(Color[] topColours, Color[] bottomColours, Color[] leftColours, Color[] righColours)
    {
        m_topColours = topColours;
        m_bottomColours = bottomColours;
        m_leftColours = leftColours;
        m_righColours = righColours;
    }
    internal int RandomMatchTop(TileColorSample[] tileSamples)
    {
        List<int> canditates = new List<int>();

        for (int k = 0; k < tileSamples.Length; k++)
        {
            if (MatchTop(tileSamples[k]))
            {
                canditates.Add(k);
            }
        }
        if (canditates.Count > 0)
        {

            int randomAmount = UnityEngine.Random.Range(0, canditates.Count);

            return canditates[randomAmount];

        }
        else
            return -1;
    }
    public bool MatchTop(TileColorSample tileSample)
    {
        int matchSampleSize = tileSample.m_bottomColours.Length;
        if (matchSampleSize != m_topColours.Length)
        {
            print("tile sample size missmatch");
            return false;
        }
        bool value = false;
        for (int i = 0; i < matchSampleSize; i++)
        {
            if (CompareColor(tileSample.getBottomAt(i), m_topColours[i]))
            {
                value = true;
            }
            else
                return false;
        }
        return value;
    }

    public bool MatchBottom(TileColorSample tileSample)
    {
        int matchSampleSize = tileSample.m_topColours.Length;
        if (matchSampleSize != m_bottomColours.Length)
        {
            print("tile sample size missmatch");
            return false;
        }
        bool value = false;
        for (int i = 0; i < matchSampleSize; i++)
        {
            if (CompareColor(tileSample.getTopAt(i), m_bottomColours[i]))
            {
                value = true;
            }
            else
                return false;
        }
        return value;
    }
    public bool MatchLeft(TileColorSample tileSample)
    {
        int matchSampleSize = tileSample.m_righColours.Length;
        if (matchSampleSize != m_leftColours.Length)
        {
            print("tile sample size missmatch");
            return false;
        }
        for (int i = 0; i < matchSampleSize; i++)
        {
            if (!CompareColor(tileSample.getRightAt(i), m_leftColours[i]))
                return false;
        }
        return true;
    }


    public int RandomMatchRight(TileColorSample[] tileSamples)
    {
        List<int> canditates = new List<int>();

        for (int k = 0; k < tileSamples.Length; k++)
        {
            if (MatchRight(tileSamples[k]))
            {
                canditates.Add(k);
            }
        }
        if (canditates.Count > 0)
        {

            int randomAmount = (int)UnityEngine.Random.Range(0, canditates.Count);

            return canditates[randomAmount];

        }
        else
            return -1;
    }
        public bool MatchRight(TileColorSample tileSample)
    {
        int matchSampleSize = tileSample.m_leftColours.Length;
        if (matchSampleSize != m_righColours.Length)
        {
            print("tile sample size missmatch");
            return false;
        }
        bool value = false;
        for (int i = 0; i < matchSampleSize; i++)
        {
            if (CompareColor(tileSample.getLeftAt(i), m_righColours[i]))
            {
                value = true;
            }
            else
            {
                return false;
            }
        }
        return value;
    }

    bool CompareColor(Color color1, Color color2)
    {
        thresholdValue = GameManager.instance.thresholdValue;

        float alpha = Math.Abs(color1.a - color2.a);
        float red = Math.Abs(color1.r - color2.r);
        float green = Math.Abs(color1.g - color2.g);
        float blue = Math.Abs(color1.b - color2.b);
        float diffSum = alpha + red + green + blue;


        if (alpha < thresholdValue && red < thresholdValue && green < thresholdValue && blue < thresholdValue && diffSum < thresholdValue)
            return true;
        else
            return false;

    }
    bool CompareColorSoft(Color color1, Color color2)
    {
        thresholdValue = GameManager.instance.thresholdValue;

        float alpha = Math.Abs(color1.a - color2.a);
        float red = Math.Abs(color1.r - color2.r);
        float green = Math.Abs(color1.g - color2.g);
        float blue = Math.Abs(color1.b - color2.b);
        float diffSum = alpha + red + green + blue;

        bool isAlphaEqual = alpha < thresholdValue;
        bool isRedEqual = red < thresholdValue;
        bool isGreenEqual = green < thresholdValue;
        bool isBlueEqual = blue < thresholdValue;

        if (alpha < thresholdValue &&  diffSum < 0.5f)
        {
            if (red < thresholdValue && green < thresholdValue)
                return true;
            if (red < thresholdValue && blue < thresholdValue)
                return true;
            if ( green < thresholdValue && blue < thresholdValue)
                return true;
        }
            return false;

    }
  
    private Color getTopAt(int i)
    {
        return m_topColours[i];
    }
    private Color getBottomAt(int i)
    {
        return m_bottomColours[i];
    }
    private Color getLeftAt(int i)
    {
        return m_leftColours[i];
    }
    private Color getRightAt(int i)
    {
        return m_righColours[i];
    }

    public override string ToString()
    {
        StringBuilder text = new StringBuilder();
        text.Append("Colors T B L R:");
        if (m_topColours.Length > 0)
            text.Append(m_topColours[m_topColours.Length / 2].ToString() + "; ");
        if (m_bottomColours.Length > 0)
            text.Append(m_bottomColours[m_bottomColours.Length / 2].ToString() + "; "); 
        if (m_leftColours.Length > 0)
            text.Append(m_leftColours[m_leftColours.Length / 2].ToString() + "; "); 
        if (m_righColours.Length > 0)
            text.Append(m_righColours[m_righColours.Length / 2].ToString() + "; ");

        return text.ToString();
    }
    internal int RandomMatchLeftBottom(TileColorSample leftPartner, TileColorSample bottomPartner, TileColorSample[] tileColorSamples)
    {
        List<int> canditates = new List<int>();


        for (int i = 0; i < tileColorSamples.Length; i++)
        {

            if (leftPartner.MatchRight(tileColorSamples[i]) && bottomPartner.MatchTop(tileColorSamples[i]))
            {
                canditates.Add(i);
            }
        }
 
        if (canditates.Count > 0)
        {

            int randomAmount = (int)UnityEngine.Random.Range(0, canditates.Count);

            return canditates[randomAmount];

        }
        print("no match");
        
            return -1;
    }
}
