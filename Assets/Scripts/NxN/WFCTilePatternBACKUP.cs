﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WFCTilePatternBACKUP : MonoBehaviour
{
    // Start is called before the first frame update

    public WFCTile[,] pattern;
    private TileEdgeDetector tileEdgeDetector;
    private TileColorSample[] tileColorSamples;
    private WangTileAutomation wangTileAutomation;

    // binary: edges left top right bottom
    private readonly int[] wangCodes = {
        0b1010011011001001,
        0b1010100110101001,
        0b1100011011000110,
        0b1100100110100110,
        0b1100011010101001,
        0b1100100111001001,
        0b1010011010100110,
        0b1010100111000110
    };
     void Start()
    {
        tileEdgeDetector = GameManager.instance.tileEdgeDetector;
        wangTileAutomation = GameManager.instance.wangTileGenerator;
        pattern = new WFCTile[3,3];
    }

    public WFCTilePatternBACKUP()
    {
        pattern = new WFCTile[3, 3];
    }
    /*
    public void SetPatternForPossibleTiles(WFCTile[] possibleTiles, WFCPattern[] tilePattern)
    {     
        //Get actual tiles for sampling:   
        tileEdgeDetector = GameManager.instance.tileEdgeDetector;
        wangTileAutomation = GameManager.instance.wangTileGenerator;
        //tileColorSamples = new TileColorSample[wangTileAutomation.GetTiles().Length];
        tileColorSamples = tileEdgeDetector.DetectEdgeColors(wangTileAutomation.GetTiles());
        int colorSampleAmount = tileColorSamples.Length;


        for (int i = 0; i < possibleTiles.Length; i++)
        {

            for (int i = 0; i < colorSampleAmount; i++)
            {
                //match left tile with middle
                if (tileColorSamples[inputTile.tileValue].MatchLeft(tileColorSamples[i]))
                {
                    pattern[0, 1].SetValue(i, 1);
                }
                else pattern[0, 1].SetValue(i, 0);

                //match right tile with middle
                if (tileColorSamples[inputTile.tileValue].MatchRight(tileColorSamples[i]))
                {
                    pattern[2, 1].SetValue(i, 1);
                }
                else pattern[2, 1].SetValue(i, 0);

                //match top tile with middle
                if (tileColorSamples[inputTile.tileValue].MatchTop(tileColorSamples[i]))
                {
                    pattern[1, 2].SetValue(i, 1);
                }
                else pattern[1, 2].SetValue(i, 0);

                //match bottom tile with middle
                if (tileColorSamples[inputTile.tileValue].MatchBottom(tileColorSamples[i]))
                {
                    pattern[1, 0].SetValue(i, 1);
                }
                else pattern[1, 0].SetValue(i, 0);


            }


        }

    }
    */
    public void SetPatternForTile(WFCTile inputTile)
    {
        //Get actual tiles for sampling:   
        tileEdgeDetector = GameManager.instance.tileEdgeDetector;
        wangTileAutomation = GameManager.instance.wangTileGenerator;
        //tileColorSamples = new TileColorSample[wangTileAutomation.GetTiles().Length];
        tileColorSamples = tileEdgeDetector.DetectEdgeColors(wangTileAutomation.GetTiles());
        int colorSampleAmount = tileColorSamples.Length;

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (i == 1 && j == 1) 
                    pattern[1, 1] = inputTile;
                else                
                    pattern[i, j] = new WFCTile(inputTile.possible_tiles.Length);
            }
        }


        for (int i = 0; i < colorSampleAmount; i++)
        {
            //match left tile with middle
            if (tileColorSamples[inputTile.tileValue].MatchLeft(tileColorSamples[i]))
            {
                pattern[0, 1].SetValue(i, 1);
            }
            else pattern[0, 1].SetValue(i, 0);

            //match right tile with middle
            if (tileColorSamples[inputTile.tileValue].MatchRight(tileColorSamples[i]))
            {
                pattern[2, 1].SetValue(i, 1);
            }
            else pattern[2, 1].SetValue(i, 0);

            //match top tile with middle
            if (tileColorSamples[inputTile.tileValue].MatchTop(tileColorSamples[i]))
            {
                pattern[1, 2].SetValue(i, 1);
            }
            else pattern[1, 2].SetValue(i, 0);

            //match bottom tile with middle
            if (tileColorSamples[inputTile.tileValue].MatchBottom(tileColorSamples[i]))
            {
                pattern[1, 0].SetValue(i, 1);
            }
            else pattern[1, 0].SetValue(i, 0);


        }
        /*
            //wangTileMap.SetTile((new Vector3Int(i - xOffset, -yOffset, 0)), tiles[matchNum]);
            //wangCodeMap[i, 0] = matchNum;


            int leftEdge = wangCodes[i] & 0b0000000011110000; //verundung mit maske für code rechter kante des linken tiles

             
            //übergabe maske für linke kante und code rechter kante des linken tiles
            //mit shift um 8 nach links damit "aus rechter kante linke kante wird"
            if ((wangCodes[inputTile.tileValue] & 0b1111000000000000) == (leftEdge << 8))
            {
                pattern[0, 1].SetValue(i,1);
            }
            else pattern[0, 1].SetValue(i, 0);

        }

        //...right...
        for (int i = 0; i < tileAmount; i++)
        {
            int rightEdge = wangCodes[i] & 0b1111000000000000; //verundung mit maske für code rechter kante des linken tiles

            //findRandomMatch(0b1111000000000000, leftEdge<<8 

            //übergabe maske für linke kante und code rechter kante des linken tiles
            //mit shift um 8 nach rechts damit "aus linker kante  rechte kante wird"
            if ((wangCodes[inputTile.tileValue] & 0b0000000011110000) == (rightEdge >> 8))
            {
                pattern[2, 1].SetValue(i, 1);
            }
            else pattern[2, 1].SetValue(i, 0);
        }

        //...top...

        for (int i = 0; i < tileAmount; i++)
        {
            int topEdge = wangCodes[i] & 0b0000000000001111; //verundung mit maske für code unterer kante des oberen tiles

            //findRandomMatch(0b1111000000000000, leftEdge<<8 

            //übergabe maske für obere kante und code unterer kante des oberen tiles
            //mit shift um 8 nach links damit "aus unterer kante obere kante wird"
            if ((wangCodes[inputTile.tileValue] & 0b0000111100000000) == (topEdge << 8))
            {
                pattern[1, 2].SetValue(i, 1);
            }
            else pattern[1, 2].SetValue(i, 0);
        }

        //...bottom...
        for (int i = 0; i < 8; i++)
        {
            int bottomEdge = wangCodes[i] & 0b0000111100000000; //verundung mit maske für code oberer kante des unteren tiles

            //findRandomMatch(0b1111000000000000, leftEdge<<8 

            //übergabe maske für unteren kante und code der oberen kante des unteren tiles
            //mit shift um 8 nach rechts damit "aus oberer kante untere kante wird"
            if ((wangCodes[inputTile.tileValue] & 0b0000000000001111) == (bottomEdge >> 8))
            {
                pattern[1, 0].SetValue(i, 1);
            }
            else pattern[1, 0].SetValue(i, 0);
        }

        */

        //match left top tile with middle



        //int leftTopEdge = wangCodes[i] & 0b0000000011111111; //verundung mit maske für code rechter und unterer kante des linken oberen tiles

        //übergabe maske für linke kante und code rechter kante des linken tiles
        //mit shift um 8 nach links damit "aus rechter kante linke kante wird"
        /*
         * 
         * 
         * 
       //gehe durch tile möglichkeiten und gleiche jeweils ALLE (!) angrenzenden möglichkeiten ab. in diesem fall: tautologie

       //Oben mitte:
       for (int j = 0; j < 8; j++)
       {

           int leftTopEdge = wangCodes[j] & 0b0000000011111111;
           for (int k = 0; k < 8; k++)
           {
               if (pattern[1, 2].possible_tiles[k] == 1)
               {
                   int leftEdge = wangCodes[k] & 0b1111000000000000;

                   if ((wangCodes[k] & 0b0000000011110000) == leftEdge >> 8)
                   {
                       pattern[0, 2].SetValue(k, 1);
                   }
                   else
                       pattern[0, 2].SetValue(k, 0);
               }

           }

           for (int i = 0; i < 8; i++)
           {

               //mitte links:
               if (pattern[0, 1].possible_tiles[i] == 1)
               {
                   int topEdge = wangCodes[i] & 0b0000111100000000;

                   if ((wangCodes[i] & 0b000000001111) == topEdge >> 8)
                   {
                       pattern[0, 2].SetValue(i, 1);
                   }
                   else
                       pattern[0, 2].SetValue(i, 0);
               }

           }

       }

       for (int i = 0; i < 8; i++)
       {
           print(pattern[1, 2].possible_tiles[i]);
       }

       //Oben rechts:
       for (int j = 0; j < 8; j++)
       {
           int rightEdge = 0b0000000000000000;
           int topEdge = 0b0000000000000000;


           int leftTopEdge = wangCodes[j] & 0b0000000011111111;
           for (int k = 0; k < 8; k++)
           {
               if (pattern[1, 2].possible_tiles[k] == 1)
               {
                   rightEdge = wangCodes[k] & 0b1111000000000000;

                   if ((wangCodes[k] & 0b0000000011110000) == rightEdge << 8)
                   {
                       pattern[0, 2].SetValue(k, 1);
                   }
                   else
                       pattern[0, 2].SetValue(k, 0);
               }

           }

           for (int i = 0; i < 8; i++)
           {
               //mitte links:
               if (pattern[0, 1].possible_tiles[i] == 1)
               {
                   topEdge = wangCodes[i] & 0b0000111100000000;

                   if ((wangCodes[i] & 0b000000001111) == topEdge >> 8)
                   {
                       pattern[0, 2].SetValue(i, 1);
                   }
                   else
                       pattern[0, 2].SetValue(i, 0);
               }

           }

       }

        */

    }

}
