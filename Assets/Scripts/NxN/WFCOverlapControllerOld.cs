﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System;

public class WFCOverlapControllerOld : MonoBehaviour
{
    public Image importedImage;
    public Image generatorImage;

    private List<WFCLocalPattern> m_localPatterns;

    private WFCEntity[,] wave;
    private WFCEntity[,] stamp;

    private List<(int, int)> anchorPoints;

    private int width;
    private int height;
    private bool isCollapsed;

    private Texture2D m_sampleTexture;
    // Start is called before the first frame update
    void Start()
    {
        isCollapsed = false;
        m_localPatterns = new List<WFCLocalPattern>();
        width = GameManager.instance.GetWidth();
        height = GameManager.instance.GetHeight();
    }

    public void GenerateTexture()
    {
        StartCoroutine(GenerateTextureAndWait());
    }

    public IEnumerator GenerateTextureAndWait()
    {
        //initialize wave:
        int width = GameManager.instance.GetWidth();
        int height = GameManager.instance.GetHeight();
        int possibilities = m_localPatterns.Count;
        wave = new WFCEntity[width, height];

        anchorPoints = new List<(int, int)>();


        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                wave[i, j] = new WFCEntity(possibilities);
                for (int k = 0; k < possibilities; k++)
                {
                    wave[i, j].possibilites[k] = 1;
                }
            }
        }

        //set initial pixel set of 2x2
        //wave[x_start, y_start] = m_localPatterns[0].pattern[0, 0];

        Color[] block = new Color[4];
        block[0] = m_localPatterns[0].pattern[0, 1];
        block[1] = m_localPatterns[0].pattern[1, 1];
        block[2] = m_localPatterns[0].pattern[0, 0];
        block[3] = m_localPatterns[0].pattern[1, 0];


        int x_start = width / 2;
        int y_start = (height / 2);

        Texture2D generatorTex = new Texture2D(width, height);

        generatorTex.filterMode = FilterMode.Point;

        //PrintWave();




        PropagatePattern(x_start, y_start, 0);

        generatorTex.SetPixels(x_start, height - 1 - y_start, 2, 2, block, 0);

        //anchorPoints.AddRange(SetAnchorPoints(x_start, y_start));
        // (int clamp_x, int clamp_y) = ClampToAnchorPoints(x_start, y_start);


        //generatorTex.SetPixel(x_start, y_start, m_localPatterns[0].pattern[0, 0]);
        //for (int i = 0; i < anchorPoints.Count; i++)
        //{
        //    generatorTex.SetPixel(anchorPoints[i].Item1, anchorPoints[i].Item2, Color.red);
        //}
        generatorTex.Apply();
        isCollapsed = false;
        generatorImage.sprite = Sprite.Create(generatorTex, new Rect(0, 0, width, height), new Vector2(0, 0));

        int counter = 0;
        while (!isCollapsed && counter < 5)
        {
            //STEP 1: find lowest entropy
            (int lowEntr_x, int lowEntr_y) = GetLowestEntropy();

            //print("lowest entropy ("+ lowEntr_x + "," + lowEntr_y + "):  " + wave[lowEntr_x, lowEntr_y].GetEntropy());

            //STEP 2: set overlap 2x2 pixels
            //bilde stempel, wende stempel auf wave an
            //OBSERVE
            int candidate = GetRandomPossibility(lowEntr_x, lowEntr_y);

            (int pos_x, int pos_y) = PlaceCandidateOnWave(lowEntr_x, lowEntr_y, candidate);
            //print("place on wave: " + pos_x + "," + pos_y + "):  " );

            //STEP 3: propagate on wave 
            //immer an einer stelle mit mindestens einmal entropy = 0
            //set of four anchor points

            //clamp lowest entropy to nearest 'lower' anchor point
            //(int clamp_x, int clamp_y) = ClampToAnchorPoints(lowestEntropy.Item1, lowestEntropy.Item2);



            // (int clamp_x, int clamp_y) = ClampToAnchorPoints(x_start, y_start);


            //generatorTex.SetPixel(x_start, y_start, m_localPatterns[0].pattern[0, 0]);




            //generatorTex.SetPixel(lowestEntropy.Item1, lowestEntropy.Item2,Color.red);




            block[0] = m_localPatterns[candidate].pattern[0, 1];
            block[1] = m_localPatterns[candidate].pattern[1, 1];
            block[2] = m_localPatterns[candidate].pattern[0, 0];
            block[3] = m_localPatterns[candidate].pattern[1, 0];

            PropagatePattern(pos_x, pos_y, candidate);
            //anchorPoints.AddRange(SetAnchorPoints(clamp_x, clamp_y));
            //print("posx: " + pos_x + " | posy: " + pos_y);
            counter++;
            //texture rectangle can get out of bounds
            if (Mathf.Abs(pos_x) >= width - 4 || Mathf.Abs(pos_y) <= 4 || Mathf.Abs(pos_x) < 4 || Mathf.Abs(pos_y) >= height - 4) continue;
            generatorTex.SetPixels(pos_x, height - 1 - pos_y, 2, 2, block, 0);

            generatorTex.Apply();
            generatorImage.sprite = Sprite.Create(generatorTex, new Rect(0, 0, width, height), new Vector2(0, 0));

            yield return new WaitForSeconds(0.1f);




            //STEP 4: if not collapsed JUMP to step 1
        }
        //yield return new WaitForSeconds(0.1f);

    }
    private (int, int) PlaceCandidateOnWave(int lowEntr_x, int lowEntr_y, int candidate)
    {
        (int pos_x, int pos_y) = (-1, -1);

        if (wave[lowEntr_x, lowEntr_y].GetAmount() == 0)
            return (pos_x, pos_y);

        if (wave[lowEntr_x - 1, lowEntr_y - 1].GetAmount() == 0 && wave[lowEntr_x - 1, lowEntr_y].GetAmount() != 0 && wave[lowEntr_x, lowEntr_y - 1].GetAmount() != 0)
            return (lowEntr_x - 1, lowEntr_y - 1); //bottom right
        else if (wave[lowEntr_x - 1, lowEntr_y - 1].GetAmount() == 0 && wave[lowEntr_x, lowEntr_y - 1].GetAmount() == 0)
            return (lowEntr_x - 1, lowEntr_y - 1); //bottom 1
        else if (wave[lowEntr_x, lowEntr_y - 1].GetAmount() == 0 && wave[lowEntr_x + 1, lowEntr_y - 1].GetAmount() == 0)
            return (lowEntr_x, lowEntr_y - 1); //bottom 2

        if (wave[lowEntr_x + 1, lowEntr_y + 1].GetAmount() == 0 && wave[lowEntr_x + 1, lowEntr_y].GetAmount() != 0 && wave[lowEntr_x, lowEntr_y + 1].GetAmount() != 0)
            return (lowEntr_x, lowEntr_y); //top left
        else if (wave[lowEntr_x, lowEntr_y + 1].GetAmount() == 0 && wave[lowEntr_x + 1, lowEntr_y + 1].GetAmount() == 0)
            return (lowEntr_x, lowEntr_y); //top 1
        else if (wave[lowEntr_x - 1, lowEntr_y + 1].GetAmount() == 0 && wave[lowEntr_x, lowEntr_y + 1].GetAmount() == 0)
            return (lowEntr_x - 1, lowEntr_y); //top 2


        if (wave[lowEntr_x - 1, lowEntr_y + 1].GetAmount() == 0 && wave[lowEntr_x - 1, lowEntr_y].GetAmount() != 0 && wave[lowEntr_x, lowEntr_y + 1].GetAmount() != 0)
            return (lowEntr_x - 1, lowEntr_y); //top right
        else if (wave[lowEntr_x - 1, lowEntr_y].GetAmount() == 0 && wave[lowEntr_x - 1, lowEntr_y + 1].GetAmount() == 0)
            return (lowEntr_x - 1, lowEntr_y); //right 1
        else if (wave[lowEntr_x - 1, lowEntr_y - 1].GetAmount() == 0 && wave[lowEntr_x - 1, lowEntr_y].GetAmount() == 0)
            return (lowEntr_x - 1, lowEntr_y - 1); //right 2

        if (wave[lowEntr_x + 1, lowEntr_y - 1].GetAmount() == 0 && wave[lowEntr_x, lowEntr_y - 1].GetAmount() != 0 && wave[lowEntr_x + 1, lowEntr_y].GetAmount() != 0)
            return (lowEntr_x - 1, lowEntr_y); //bottom left
        else if (wave[lowEntr_x + 1, lowEntr_y].GetAmount() == 0 && wave[lowEntr_x + 1, lowEntr_y + 1].GetAmount() == 0)
            return (lowEntr_x, lowEntr_y); //left 1
        else if (wave[lowEntr_x + 1, lowEntr_y - 1].GetAmount() == 0 && wave[lowEntr_x + 1, lowEntr_y].GetAmount() == 0)
            return (lowEntr_x, lowEntr_y - 1); //left2




        return (pos_x, pos_y);

    }
    private (int, int) PlaceCandidateOnTypeWave(int lowEntr_x, int lowEntr_y, int candidate)
    {
        (int pos_x, int pos_y) = (0, 0);
        WFCEntity.BorderType type = wave[lowEntr_x, lowEntr_y].entityType;

        switch (type)
        {
            case WFCEntity.BorderType.TopLeft:
                (pos_x, pos_y) = (lowEntr_x, lowEntr_y);

                break;
            case WFCEntity.BorderType.Top:
                (pos_x, pos_y) = (lowEntr_x - 1, lowEntr_y);

                break;
            case WFCEntity.BorderType.TopRight:
                (pos_x, pos_y) = (lowEntr_x - 1, lowEntr_y);

                break;
            case WFCEntity.BorderType.Right:
                (pos_x, pos_y) = (lowEntr_x - 1, lowEntr_y);

                break;
            case WFCEntity.BorderType.BottomRight:
                (pos_x, pos_y) = (lowEntr_x - 1, lowEntr_y - 1);
                break;
            case WFCEntity.BorderType.Bottom:
                (pos_x, pos_y) = (lowEntr_x, lowEntr_y - 1);
                break;
            case WFCEntity.BorderType.BottomLeft:
                (pos_x, pos_y) = (lowEntr_x, lowEntr_y - 1);
                break;
            case WFCEntity.BorderType.Left:
                (pos_x, pos_y) = (lowEntr_x, lowEntr_y);
                break;
            case WFCEntity.BorderType.Collapsed:
                (pos_x, pos_y) = (-1, -1);
                break;
            default:
                break;
        }
        return (pos_x, pos_y);
    }

    private (int, int) ClampToAnchorPoints(int x, int y)
    {

        (int, int) closestAnchor = anchorPoints[0];

        for (int i = 0; i < anchorPoints.Count; i++)
        {
            int distx = closestAnchor.Item1 - anchorPoints[i].Item1;
            int disty = closestAnchor.Item2 - anchorPoints[i].Item2;

            //if "above" and distance smaller than 2
            if (distx > 0 && disty > 0 && Mathf.Abs(distx) < 2 && Mathf.Abs(disty) < 2)
            {
                return anchorPoints[i];
            }
        }
        return closestAnchor;
    }

    private int GetRandomPossibility(int x_pos, int y_pos)
    {
        List<int> canditates = new List<int>();

        for (int i = 0; i < wave[x_pos, y_pos].possibilites.Length; i++)
        {
            if (wave[x_pos, y_pos].possibilites[i] == 1)
            {
                canditates.Add(i);
            }
        }
        if (canditates.Count > 0)
        {

            int randomAmount = UnityEngine.Random.Range(0, canditates.Count);

            return canditates[randomAmount];

        }
        else
            return 0;
    }

    private (int, int) GetLowestEntropy()
    {
        isCollapsed = true;
        float lowestEntropyValue = 1.0f;
        (int, int) lowestEntropyPos = (0, 0);
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (!wave[i, j].isCollapsed) isCollapsed = false;
                //else wave[i, j].Clear();
                if (!wave[i, j].isCollapsed && wave[i, j].GetAmount() > 0 && lowestEntropyValue > wave[i, j].GetEntropy())
                {
                    lowestEntropyPos = (i, j);
                    lowestEntropyValue = wave[i, j].GetEntropy();
                }

            }

        }

        return lowestEntropyPos;
    }

    private (int, int)[] SetAnchorPoints(int x_pos, int y_pos)
    {
        (int, int)[] anchorPoints = new (int, int)[4];

        anchorPoints[0] = (x_pos - 1, y_pos - 1);
        anchorPoints[1] = (x_pos + 1, y_pos - 1);
        anchorPoints[2] = (x_pos - 1, y_pos + 1);
        anchorPoints[3] = (x_pos + 1, y_pos + 1);
        return anchorPoints;
    }

    private void PropagatePattern(int x_pos, int y_pos, int v)
    {
        int possibilites = m_localPatterns.Count;



        wave[x_pos, y_pos].isCollapsed = true;
        //
        //int topLeft = x_pos, y_pox

        if (x_pos + 1 < width)
        {
            wave[x_pos + 1, y_pos].isCollapsed = true;
        }
        if (y_pos + 1 < height)
        {
            wave[x_pos, y_pos + 1].isCollapsed = true;
        }
        if (y_pos + 1 < height && x_pos + 1 < width)
        {
            wave[x_pos + 1, y_pos + 1].isCollapsed = true;
        }


        //bilde durch veroderung innerhalb der abmessungen einen stempel der mit der wave verundet werden kann


        //von (-1,-1) zu (+1, +1) wenn möglich

        //Stempelabmessungen. Wenn am Rand, dann innerhalb:
        int x_min = x_pos - 1;
        int y_min = y_pos - 1;
        int x_max = x_pos + 3;
        int y_max = y_pos + 3;

        //if not touching borders, max - min is 4 in both dimensions:
        stamp = new WFCEntity[(x_max - x_min), (y_max - y_min)];
        //print("range x: " +(x_max - x_min)+ " | range y: " + (y_max - y_min));
        //initialize stamp field
        for (int i = 0; i < (x_max - x_min); i++)
        {
            for (int j = 0; j < (y_max - y_min); j++)
            {
                stamp[i, j] = new WFCEntity(m_localPatterns.Count);
            }
        }
        //PrintWave();
        //DEBUG:
        // return;

        //match all the things
        //wfcOverlapIndex[j, i].Count    
        for (int j = 0; j < 3; j++)
        {
            if (y_min + j > 0 && y_max + j < height)
                for (int i = 0; i < 3; i++)
                {
                    if (i == 1 && j == 1) continue;

                    if (x_min + i > 0 && x_max + i < width)
                    {
                        for (int k = 0; k < possibilites; k++)
                        {
                            //mittleres pattern (1|1) sollte leer sein:
                            //print("m_localPatterns[v]  amount: " + m_localPatterns.Count + "|i: " + i + " |j: " + j);
                            //print("overlap index amount: " + m_localPatterns[v].wfcOverlapIndex[i, j].Length);
                            //print("overlap index amount: " + m_localPatterns[v].wfcOverlapIndex[i, j].Length);

                            //if (m_localPatterns[v]?.wfcOverlapIndex[i, j][k] == 0)         


                            // pattern k in zu i und j korrespondierendem sektor nicht möglich
                            //-> einschränkung weitergeben

                            //oben links                               

                            // veroderung |
                            //print(stamp[i + 1, j].possibilites[k]);
                            //print(stamp[i, j + 1].possibilites[k]);
                            //print(stamp[i + 1, j + 1].possibilites[k]);
                            //if (i == 2 && j == 0)
                            //    print("test case 2,0: " + (m_localPatterns[v].wfcOverlapIndex[i, j][k] | stamp[i, j].possibilites[k]));

                            //stamp[i, j].possibilites[k] |= m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            //stamp[i + 1, j].possibilites[k] |= m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            //stamp[i , j + 1].possibilites[k] |= m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            //stamp[i + 1, j + 1].possibilites[k] |= m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            /*
                            stamp[i, j].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, 2 - j][k];
                            stamp[i + 1, j].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, 2 - j][k];
                            stamp[i, j + 1].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, 2 - j][k];
                            stamp[i + 1, j + 1].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, 2 - j][k];
                            */
                            //PrintStamp((x_max - x_min), (y_max - y_min));




                            //unten rechts
                            //if (i == 2 && j == 2)
                            //{
                            //    // veroderung |
                            //    stamp[i, j].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            //    // verundung &                                    
                            //    wave[x_min + i, y_min + j].possibilites[k] &= stamp[i, j].possibilites[k];
                            //}





                            //Setting up border types:





                            // 0,1,2 0,1,2
                            if (i == 0 && j == 0)
                            {
                                stamp[i, j].entityType = WFCEntity.BorderType.TopLeft;
                                stamp[i + 1, j].entityType = WFCEntity.BorderType.Top;
                                stamp[i, j + 1].entityType = WFCEntity.BorderType.Left;
                                stamp[i + 1, j + 1].entityType = WFCEntity.BorderType.Collapsed;

                                stamp[i, j].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            }
                            else if (i == 1 && j == 0)
                            {
                                stamp[i, j].entityType = WFCEntity.BorderType.Top;
                                stamp[i + 1, j].entityType = WFCEntity.BorderType.Top;
                                stamp[i, j + 1].entityType = WFCEntity.BorderType.Collapsed;
                                stamp[i + 1, j + 1].entityType = WFCEntity.BorderType.Collapsed;

                                stamp[i, j].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                                stamp[i + 1, j].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];

                            }
                            else if (i == 2 && j == 0)
                            {
                                stamp[i, j].entityType = WFCEntity.BorderType.Top;
                                stamp[i + 1, j].entityType = WFCEntity.BorderType.TopRight;
                                stamp[i, j + 1].entityType = WFCEntity.BorderType.Collapsed;
                                stamp[i + 1, j + 1].entityType = WFCEntity.BorderType.Right;

                                stamp[i + 1, j].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            }
                            else if (i == 0 && j == 1)
                            {
                                stamp[i, j].entityType = WFCEntity.BorderType.Left;
                                stamp[i + 1, j].entityType = WFCEntity.BorderType.Collapsed;
                                stamp[i, j + 1].entityType = WFCEntity.BorderType.Left;
                                stamp[i + 1, j + 1].entityType = WFCEntity.BorderType.Collapsed;

                                stamp[i, j].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                                stamp[i, j + 1].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            }
                            else if (i == 0 && j == 2)
                            {
                                stamp[i, j].entityType = WFCEntity.BorderType.Left;
                                stamp[i + 1, j].entityType = WFCEntity.BorderType.Collapsed;
                                stamp[i, j + 1].entityType = WFCEntity.BorderType.BottomLeft;
                                stamp[i + 1, j + 1].entityType = WFCEntity.BorderType.Bottom;

                                stamp[i, j + 1].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            }
                            else if (i == 1 && j == 2)
                            {
                                stamp[i, j].entityType = WFCEntity.BorderType.Collapsed;
                                stamp[i + 1, j].entityType = WFCEntity.BorderType.Collapsed;
                                stamp[i, j + 1].entityType = WFCEntity.BorderType.Bottom;
                                stamp[i + 1, j + 1].entityType = WFCEntity.BorderType.Bottom;

                                stamp[i, j + 1].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                                stamp[i + 1, j + 1].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            }
                            else if (i == 2 && j == 2)
                            {
                                stamp[i, j].entityType = WFCEntity.BorderType.Collapsed;
                                stamp[i + 1, j].entityType = WFCEntity.BorderType.Right;
                                stamp[i, j + 1].entityType = WFCEntity.BorderType.Bottom;
                                stamp[i + 1, j + 1].entityType = WFCEntity.BorderType.BottomRight;

                                stamp[i + 1, j + 1].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            }
                            else if (i == 2 && j == 1)
                            {
                                stamp[i, j].entityType = WFCEntity.BorderType.Collapsed;
                                stamp[i + 1, j].entityType = WFCEntity.BorderType.Right;
                                stamp[i, j + 1].entityType = WFCEntity.BorderType.Collapsed;
                                stamp[i + 1, j + 1].entityType = WFCEntity.BorderType.Right;

                                stamp[i + 1, j].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                                stamp[i + 1, j + 1].possibilites[k] = m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            }
                            //print("i:" + i + "| j: " + j);

                            if (stamp[i, j].isCollapsed)
                            {
                                stamp[i, j].entityType = WFCEntity.BorderType.Collapsed;
                            }
                            if (stamp[i + 1, j].isCollapsed)
                            {
                                stamp[i + 1, j].entityType = WFCEntity.BorderType.Collapsed;
                            }
                            if (stamp[i, j + 1].isCollapsed)
                            {
                                stamp[i, j + 1].entityType = WFCEntity.BorderType.Collapsed;
                            }
                            if (stamp[i + 1, j + 1].isCollapsed)
                            {
                                stamp[i + 1, j + 1].entityType = WFCEntity.BorderType.Collapsed;
                            }

                        }
                    }
                }
        }
        for (int j = 0; j < 3; j++)
        {
            if (y_min + j > 0 && y_max + j < height)
                for (int i = 0; i < 3; i++)
                {
                    if (i == 1 && j == 1) continue;

                    if (x_min + i > 0 && x_max + i < width)
                        for (int k = 0; k < possibilites; k++)
                        {
                            // verundung &                                    
                            wave[x_min + i, y_min + j].possibilites[k] &= stamp[i, j].possibilites[k];
                            wave[x_min + i + 1, y_min + j].possibilites[k] &= stamp[i + 1, j].possibilites[k];
                            wave[x_min + i, y_min + j + 1].possibilites[k] &= stamp[i, j + 1].possibilites[k];
                            wave[x_min + i + 1, y_min + j + 1].possibilites[k] &= stamp[i + 1, j + 1].possibilites[k];

                            if (wave[x_min + i, y_min + j].entityType != WFCEntity.BorderType.Collapsed) wave[x_min + i, y_min + j].entityType = stamp[i, j].entityType;
                            if (wave[x_min + i + 1, y_min + j].entityType != WFCEntity.BorderType.Collapsed) wave[x_min + i + 1, y_min + j].entityType = stamp[i + 1, j].entityType;
                            if (wave[x_min + i, y_min + j + 1].entityType != WFCEntity.BorderType.Collapsed) wave[x_min + i, y_min + j + 1].entityType = stamp[i, j + 1].entityType;
                            if (wave[x_min + i + 1, y_min + j + 1].entityType != WFCEntity.BorderType.Collapsed) wave[x_min + i + 1, y_min + j + 1].entityType = stamp[i + 1, j + 1].entityType;

                        }
                }
        }

        //PrintTypeWave();
        PrintWave();
    }
    public void PrintStamp(int x, int y)
    {
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                strBuilder.Append(stamp[i, j].GetEntropy());

                strBuilder.Append("|");
            }
            strBuilder.AppendLine();
        }
        print(strBuilder.ToString());
    }

    public void PrintWave()
    {
        StringBuilder strBuilder = new StringBuilder();
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {  /*
                float entropy = wave[i, j].GetEntropy();
                if (entropy == 1 | entropy == 0)
                    strBuilder.Append(" ");
                entropy *= 10;
                int entropyInt = (int) entropy;

                entropy = (float) entropyInt / 10;
 
                strBuilder.Append(entropy);
                if (entropy == 1 | entropy == 0)
                    strBuilder.Append("  ");
                    */

                int amount = wave[i, j].GetAmount();

                if (amount < 10)
                    strBuilder.Append(" ");

                strBuilder.Append(amount);
                if (amount < 10)
                    strBuilder.Append("  ");
                strBuilder.Append("|");
            }
            strBuilder.AppendLine();
        }
        print(strBuilder.ToString());
    }
    public void PrintTypeWave()
    {
        StringBuilder strBuilder = new StringBuilder();
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {  /*
                float entropy = wave[i, j].GetEntropy();
                if (entropy == 1 | entropy == 0)
                    strBuilder.Append(" ");
                entropy *= 10;
                int entropyInt = (int) entropy;

                entropy = (float) entropyInt / 10;
 
                strBuilder.Append(entropy);
                if (entropy == 1 | entropy == 0)
                    strBuilder.Append("  ");
                    */

                WFCEntity.BorderType type = wave[i, j].entityType;
                strBuilder.Append("  ");
                strBuilder.Append(type);
                strBuilder.Append("|");
            }
            strBuilder.AppendLine();
        }
        print(strBuilder.ToString());
    }
    public void PatternsFromImportedImage()
    {
        if (importedImage?.sprite == null)
        {
            print("image null");
            return;
        }
        //m_sampleTexture = importedImage.sprite.texture;

        if (m_sampleTexture?.width > 0)
        {
            print("source tex height: " + m_sampleTexture.height + "| source tex width: " + m_sampleTexture.width);

            //extract local patterns:
            m_localPatterns.Clear();

            int y = m_sampleTexture.height;
            for (int j = 1; j < y; j++)
            {
                for (int i = 0; i < m_sampleTexture.width - 1; i++)
                {
                    WFCLocalPattern tmpPattern = new WFCLocalPattern();
                    tmpPattern.pattern[0, 0] = m_sampleTexture.GetPixel(i, y - j);
                    tmpPattern.pattern[1, 0] = m_sampleTexture.GetPixel(i + 1, y - j);
                    tmpPattern.pattern[0, 1] = m_sampleTexture.GetPixel(i, y - j - 1);
                    tmpPattern.pattern[1, 1] = m_sampleTexture.GetPixel(i + 1, y - j - 1);
                    //print("tmpPattern.pattern[0,0]: " + tmpPattern.pattern[0, 0]);
                    if (!CompareCandidateToList(m_localPatterns, tmpPattern))
                        m_localPatterns.Add(tmpPattern);
                }
            }
            //PrintPatterns();

            print("source pattern amount: " + m_localPatterns.Count);

            //REFLECTION AND ROTATION
            //rotate 3 times and add each step (ONLY in N=2)  
            int sourcePatternAmount = m_localPatterns.Count;
            for (int i = 0; i < sourcePatternAmount; i++)
            {
                WFCLocalPattern tempPattern01 = m_localPatterns[i].RotateClockwise(1);

                WFCLocalPattern tempPattern02 = m_localPatterns[i].RotateClockwise(2);

                WFCLocalPattern tempPattern03 = m_localPatterns[i].RotateClockwise(3);

                if (!CompareCandidateToList(m_localPatterns, tempPattern01))
                    m_localPatterns.Add(tempPattern01);
                if (!CompareCandidateToList(m_localPatterns, tempPattern02))
                    m_localPatterns.Add(tempPattern02);
                if (!CompareCandidateToList(m_localPatterns, tempPattern03))
                    m_localPatterns.Add(tempPattern03);
            }
            //PrintPatterns();

            print("pattern amount after rotation : " + m_localPatterns.Count);
            for (int i = 0; i < m_localPatterns.Count; i++)
            {
                m_localPatterns[i].CreateOverlapIndex(m_localPatterns);
                //m_localPatterns[i].PrintOverlapAmount();
            }

            //  if (localPatterns.Count > 0)
            // print("pattern example: " + localPatterns[0].PrintPattern());
            //Get rid of duplicates:
        }

    }

    public void PrintPatterns()
    {

        for (int i = 0; i < m_localPatterns.Count; i++)
        {
            print(m_localPatterns[i].PrintPattern());
            print("............................................................");
        }

    }
    public bool CompareCandidateToList(List<WFCLocalPattern> localPatterns, WFCLocalPattern candidate)
    {
        for (int i = 0; i < localPatterns.Count; i++)
        {
            if (localPatterns[i].Equals(candidate))
                return true;
        }
        return false;
    }
    internal void SetImportImage(Texture2D sampleTexture)
    {
        if (sampleTexture != null)
            m_sampleTexture = sampleTexture;
    }
}
