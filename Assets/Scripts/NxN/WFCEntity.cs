﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WFCEntity : MonoBehaviour
{
    public int[] possibilites;
    public bool isCollapsed;
    public BorderType entityType;
    public bool needsToBeUpdated;
    public enum BorderType
    {   O,
        TopLeft,    
        Top,
        TopRight,
        Right,
        BottomRight,
        Bottom,
        BottomLeft,
        Left,
        Collapsed
    }

    public WFCEntity(int value)
    {
        needsToBeUpdated = false;
        isCollapsed = false;
        possibilites = new int[value];

        for (int i = 0; i < possibilites.Length; i++)
        {
            possibilites[i] = 1;
        }
    }
    public WFCEntity(int value, int flag)
    {
        needsToBeUpdated = false;
        isCollapsed = false;
        possibilites = new int[value];

        for (int i = 0; i < possibilites.Length; i++)
        {
            possibilites[i] = flag;
        }
    }

    public float GetEntropy() {
        //if (isCollapsed)
        //    return 0f;
        //else {
        //    if (possibilites.Length < 2)
        //        return 0f;
        //    float sum = 0f;
        //    for (int i = 0; i < possibilites.Length; i++)
        //    {
        //        sum += possibilites[i];
        //    }

        //    return sum / possibilites.Length;
        //}  
        if (isCollapsed)
            return 0;
        return (float) GetAmount() / possibilites.Length;
    }

    internal int GetAmount()
    {
        if (isCollapsed)
            return 1;
        else
        {
            int sum = 0;
            for (int i = 0; i < possibilites.Length; i++)
            {
                sum += possibilites[i];
            }

            return sum;
        }
    }

    internal void Clear()
    {
        for (int i = 0; i < possibilites.Length; i++)
        {
            possibilites[i] = 0;
        }

    }
}
