﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class WFCLocalPatternNxN : MonoBehaviour
{
    public Color[,] pattern;

    public int[,][] wfcOverlapIndex;

    private int m_n;
    private int sampleAmount;
    private int sampleAmount_xy;

    private int m_patternAmount; 
    // Start is called before the first frame update
    void Start()
    {
        m_n = 2;

        m_patternAmount = 0;
        // sampleAmount = (2n - 1)^2
        sampleAmount = (2 * m_n - 1) * (2 * m_n - 1);
        sampleAmount_xy = (2 * m_n - 1);

        pattern = new Color[m_n,m_n];
        wfcOverlapIndex = new int[sampleAmount_xy, sampleAmount_xy][];
        //initializing local pattern with white
        for (int i = 0; i < m_n; i++)
        {
            for (int k = 0; k < m_n; k++)
            {
                pattern[i, k] = Color.white;
            }

        }

    }

    public void CreateOverlapIndex(List<WFCLocalPatternNxN> localPatterns)
    {

        //print("calculate overlaps NxN");
        //print("m_n: " + m_n);
        //HARD CODED:
        sampleAmount_xy = 3;
        int overlapCount = sampleAmount_xy;

        wfcOverlapIndex = new int[overlapCount, overlapCount][];

        for (int i = 0; i < overlapCount; i++)
        {
            for (int j = 0; j < overlapCount; j++)
            {
                wfcOverlapIndex[i, j] = new int[localPatterns.Count];
            }
        }
        for (int i = 0; i < overlapCount; i++)
        {
            for (int j = 0; j < overlapCount; j++)
            {
                for (int k = 0; k < localPatterns.Count; k++)
                {
                    bool isSame = true;

                    for (int x = 0; x < m_n; x++)
                    {
                        for (int y = 0; y < m_n; y++)
                        {
                 
                                    int local_x = - x + (m_n - 1) ;
                                    int local_y = - y + (m_n - 1) ;
                                    if (local_x > 0 && local_x < m_n && local_y > 0 && local_y < m_n)
                                        if (pattern[x, y] != localPatterns[k].pattern[local_x, local_y])
                                            isSame = false;
                                
                                
                            
                        }
                    }

                    if (isSame)
                    {
                        wfcOverlapIndex[i, j][k] = 1;
                    }
                    else
                        wfcOverlapIndex[i, j][k] = 0;
                }
            }
        }

        PrintOverlapAmount();

        return;

        for (int i = 0; i < overlapCount; i++)
        {

            for (int j = 0; j < overlapCount; j++)
            {
                // i: 0-4

                //for x < 3/ y < 3 x++ y++
                // if overlap [i - (n-1), ...]
                //if( i - (n-1) + x >= 0 &&)
                //for pattern amount:

                // case n=2 overlap [i - 1 + x, ...] == pattern[x,y]
                // case n=3 overlap [i - 2, ...]


                //i = 4
                // if overlap [4 - (n-1), ...]
                //if( 4 - (n-1) + x >= 0 &&)
                // case n=2 overlap [4 - 1 + x, ...] == pattern[x,y]

                //
                for (int k = 0; k < localPatterns.Count; k++)
                {
                    for (int x = 0; x < m_n; x++)
                    {
                        for (int y = 0; y < m_n; y++)
                        {
                            if (i - (m_n - 1) + x >= 0 && i - (m_n - 1) + x < m_n && j - (m_n - 1) + y >= 0 && j - (m_n - 1) + y < m_n)
                            {
                                bool isSame = true;
                         
                                
                                    //localPatterns[x,y].pattern

                       
                          
                                if (pattern[i - (m_n - 1) + x, j - (m_n - 1) + y] == localPatterns[k].pattern[x, y])
                                {

                                    //print("i - (m_n - 1) + x: " + (i - (m_n - 1) + x) + " j - (m_n - 1) + y : " + (j - (m_n - 1) + y));
                                    //print("x: " + x + " | y: " + y);
                                }
                                else isSame = false;
                                //print("pattern[i - (m_n - 1) + x, j - (m_n - 1) + y] : " + pattern[i - (m_n - 1) + x, j - (m_n - 1) + y]);
                                //print("localPattern: " + localPatterns[k].pattern[x,y]);

                            
                           
                                if (isSame)
                                {

                                    //print("i - (m_n - 1) + x: " + (i - (m_n - 1) + x) + " j - (m_n - 1) + y : " + (j - (m_n - 1) + y));
                                    //print("x: " + x + " | y: " + y);
                                    wfcOverlapIndex[i, j][k] = 1;

                                }
                                else 
                                    wfcOverlapIndex[i, j][k] = 0;

                            }
                        }
                    }
                }

 
            }

        }

        PrintOverlapAmount();

        ////Top left(-1, -1): [0,0] of tile with [1,1] of every other tile
        //int[] listTopLeft = MatchTwoCandidates(localPatterns, 0, 0, 1, 1);
        ////Top right(+1, -1): [1,0] of tile with [0,1] of every other tile
        //int[] listTopRight = MatchTwoCandidates(localPatterns, 1, 0, 0, 1);
        ////Bottom right(+1, +1): [1,1] of tile with [0,0] of every other tile
        //int[] listBottomRight = MatchTwoCandidates(localPatterns, 1, 1, 0, 0);
        ////Bottom left(-1, +1): [0,1] of tile with [1,0] of every other tile
        //int[] listBottomLeft = MatchTwoCandidates(localPatterns, 0, 1, 1, 0);

        ////Top [0,0] [1,0] of tile with [0,1][1,1] of every other tile
        //int[] listTop = MatchTwoCandidatesDual(localPatterns, 0, 0, 1, 0, 0, 1, 1, 1);
        ////Right [1,0] [1,1] of tile with [0,0][0,1] of every other tile
        //int[] listRight = MatchTwoCandidatesDual(localPatterns, 1, 0, 1, 1, 0, 0, 0, 1);
        ////Bottom [0,1] [1,1] of tile with [0,0][1,0] of every other tile
        //int[] listBottom = MatchTwoCandidatesDual(localPatterns, 0, 1, 1, 1, 0, 0, 1, 0);
        ////left [0,0] [0,1] of tile with [1,0][1,1] of every other tile
        //int[] listLeft = MatchTwoCandidatesDual(localPatterns, 0, 0, 0, 1, 1, 0, 1, 1);


        ////wfcOverlapIndex[0, 0] = listTopLeft;
        //wfcOverlapIndex[1, 0] = listTop;
        //wfcOverlapIndex[2, 0] = listTopRight;
        //wfcOverlapIndex[0, 1] = listLeft;
        //wfcOverlapIndex[0, 2] = listBottomLeft;
        //wfcOverlapIndex[1, 2] = listBottom;
        //wfcOverlapIndex[2, 2] = listBottomRight;
        //wfcOverlapIndex[2, 1] = listRight;

        //PrintOverlapAmount();
    }

    public void PrintOverlapAmount() {

        StringBuilder strBuilder = new StringBuilder();
        //print("sampleAmount_xy: " + sampleAmount_xy);
        //HARD CODED:
        sampleAmount_xy = (2 * m_n - 1);
        m_patternAmount = wfcOverlapIndex[0, 0].Length;

        for (int i = 0; i < sampleAmount_xy; i++)
        {
            for (int j  = 0; j < sampleAmount_xy; j++)
            {
 
                    int amount = 0;

                    for (int k = 0; k < m_patternAmount; k++)
                    {
                        amount += wfcOverlapIndex[j, i][k];
                    }
                    strBuilder.Append(amount);
                    strBuilder.Append("|");
           
            }
            strBuilder.Append("\n");
        }


        print(strBuilder.ToString());
    }

    int[] MatchTwoCandidates(List<WFCLocalPatternNxN> localPatterns, int x0,int y0, int x1, int y1)
    {
        int[] matches = new int[localPatterns.Count];
        for (int i = 0; i < localPatterns.Count; i++)
        {
            if (localPatterns[i].pattern[x1, y1] == pattern[x0, y0])
            {
                matches[i] = 1;
                //print("localPatterns[" + i + "].pattern[--]\n " + localPatterns[i].PrintPattern());
            }
            else
                matches[i] = 0;
        }
        //print("[" + x0 + "," + y0 + "] match amount: " + matches.Count);
        //
        return matches;
    }
    int[] MatchTwoCandidatesDual(List<WFCLocalPatternNxN> localPatterns, int x0a, int y0a, int x0b, int y0b, int x1a, int y1a, int x1b, int y1b)
    {
        int[] matches = new int[localPatterns.Count];

        for (int i = 0; i < localPatterns.Count; i++)
        {
            if (localPatterns[i].pattern[x1a, y1a] == pattern[x0a, y0a] && localPatterns[i].pattern[x1b, y1b] == pattern[x0b, y0b])
            {
                matches[i] = 1;
                //print("localPatterns[" + i + "].pattern[--]\n " + localPatterns[i].PrintPattern());
            }
            else
                matches[i] = 0;
        }
        //print("[" + x0a + "," + y0a + "]/[" + x0b + "," + y0b + "] match amount: " + matches.Count);
        //
        return matches;
    }

    public WFCLocalPatternNxN(int patternAmount)
    {
        pattern = new Color[m_n, m_n];
        m_patternAmount = patternAmount;
        //initializing local pattern with white
        for (int i = 0; i < m_n; i++)
        {
            for (int k = 0; k < m_n; k++)
            {
                pattern[i, k] = Color.white;
            }

        }

        wfcOverlapIndex = new int[3, 3][];

        for (int i = 0; i < 3; i++)
        {
            for (int k = 0; k < 3; k++)
            {
                wfcOverlapIndex[i, k] = new int[patternAmount];
                for (int j = 0; j < patternAmount; j++)
                {
                    wfcOverlapIndex[i, k][j] = 0;
                }
            }

        }
    }

    public WFCLocalPatternNxN(int patternAmount, int n)
    {
         // sampleAmount = (2n - 1)^2
        sampleAmount = (2 * n - 1) * (2 * n - 1);
        sampleAmount_xy = (2 * n - 1);
        m_patternAmount = patternAmount;
        pattern = new Color[n, n];
        //initializing local pattern with white
        for (int i = 0; i < n; i++)
        {
            for (int k = 0; k < n; k++)
            {
                pattern[i, k] = Color.white;
            }

        }

        wfcOverlapIndex = new int[sampleAmount_xy, sampleAmount_xy][];

        for (int i = 0; i < sampleAmount_xy; i++)
        {
            for (int k = 0; k < sampleAmount_xy; k++)
            {
                wfcOverlapIndex[i, k] = new int[patternAmount];
                for (int j = 0; j < patternAmount; j++)
                {
                    wfcOverlapIndex[i, k][j] = 0;
                }
            }

        }
    }


    public WFCLocalPatternNxN()
    {
        //HARD CODED:
        m_n = 2;

        pattern = new Color[m_n, m_n];
       // print("n = " + m_n);
        //initializing local pattern with white
        for (int i = 0; i < m_n; i++)
        {
            for (int k = 0; k < m_n; k++)
            {
                pattern[i, k] = Color.white;
            }

        }
        wfcOverlapIndex = new int[sampleAmount_xy, sampleAmount_xy][];
        //initializing local pattern with white
        for (int i = 0; i < sampleAmount_xy; i++)
        {
            for (int k = 0; k < sampleAmount_xy; k++)
            {
                wfcOverlapIndex[i, k] = null;
            }
        }
    }

    public WFCLocalPatternNxN(Color[,] inputPattern)
    {
        //HARD CODED
        m_n = 2;

        pattern = new Color[m_n, m_n];

        //initializing local pattern with white
        for (int i = 0; i < m_n; i++)
        {
            for (int k = 0; k < m_n; k++)
            {
                pattern[i, k] = inputPattern[i,k];
            }

        }
    }
    public void CopyPattern(Color[,] copyPattern, Color[,] replacePattern)
    {
        for (int i = 0; i < m_n; i++)
        {
            for (int k = 0; k < m_n; k++)
            {
                replacePattern[i, k] = copyPattern[i, k];
            }

        }
    }

    internal string PrintPattern()
    {
        StringBuilder strBuilder = new StringBuilder();

       for (int k = 0; k < m_n; k++)
            {
            for (int i = 0; i < m_n; i++)
            {
                strBuilder.Append(" | " + pattern[i, k].ToString());
            }
            strBuilder.Append("\n");
        }
        return strBuilder.ToString();
    }
    internal string PrintSingleColor(int x, int y)
    {
        return pattern[x, y].ToString();
    }

    internal WFCLocalPatternNxN RotateClockwise(int count)
    {

        WFCLocalPatternNxN srcWFCLocalPattern = new WFCLocalPatternNxN(pattern);
        WFCLocalPatternNxN tempWFCLocalPattern = new WFCLocalPatternNxN(pattern);

        for (int i = 0; i < count; i++)
        {
            tempWFCLocalPattern.pattern[0, 0] = srcWFCLocalPattern.pattern[1, 0];
            tempWFCLocalPattern.pattern[1, 0] = srcWFCLocalPattern.pattern[1, 1];
            tempWFCLocalPattern.pattern[0, 1] = srcWFCLocalPattern.pattern[0, 0];
            tempWFCLocalPattern.pattern[1, 1] = srcWFCLocalPattern.pattern[0, 1];

            CopyPattern(tempWFCLocalPattern.pattern, srcWFCLocalPattern.pattern);
        }


        return tempWFCLocalPattern;
    }


    public   bool __Equals(object obj)
    {
        WFCLocalPatternNxN candidate = (WFCLocalPatternNxN)obj;

        return
        candidate.pattern[0, 0] == pattern[0, 0] &&
        candidate.pattern[1, 0] == pattern[1, 0] &&
        candidate.pattern[0, 1] == pattern[0, 1] &&
        candidate.pattern[1, 1] == pattern[1, 1];

    }


    public override bool Equals(object obj)
    {
        WFCLocalPatternNxN candidate = (WFCLocalPatternNxN)obj;

        //if (obj.GetType() != candidate.GetType()) return false;

        bool isEqual = true;

        for (int i = 0; i < m_n; i++)
            for (int j = 0; j < m_n; j++) { 
                if (!candidate.pattern[i, j].Equals(pattern[i, j]))
                {
                    isEqual = false;
                } 
            }

        return isEqual;
    }
}
