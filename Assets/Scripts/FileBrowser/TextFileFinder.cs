﻿using UnityEngine;

public class TextFileFinder : MonoBehaviour
{
    public GameObject button;
    public string m_textPath;

    protected FileBrowser m_fileBrowser;

    [SerializeField]
    protected Texture2D m_directoryImage,
                        m_fileImage;

 
    protected void OnGUI()
    {
        if (m_fileBrowser != null)
        {
            m_fileBrowser.OnGUI();
        }
        else
        {
            OnGUIMain();
        }
    }

    protected void OnGUIMain()
    {

        GUILayout.BeginHorizontal();
        GUILayout.Label("Import png-File", GUILayout.Width(100));
        GUILayout.FlexibleSpace();
        GUILayout.Label(m_textPath ?? "none selected");
        if (GUILayout.Button("...", GUILayout.ExpandWidth(false)))
        {
            m_fileBrowser = new FileBrowser(
                new Rect(50, 50, 600, 500),
                "Choose png-File",
                FileSelectedCallback
            );
            m_fileBrowser.SelectionPattern = "*.png";
            m_fileBrowser.DirectoryImage = m_directoryImage;
            m_fileBrowser.FileImage = m_fileImage;

        }
        GUILayout.EndHorizontal();
    }

    protected void FileSelectedCallback(string path)
    {
        m_fileBrowser = null;
        m_textPath = path;
        if(button != null)
        button.SetActive(true);

        //Load UI Tile Palette Display:
        //GameManager.instance.InvokeUIPaletteLoading();
        //print(m_textPath);
        if(m_textPath != null && m_textPath.Length > 0)
        GameManager.instance.InvokeUIImportImagePreviewLoading(m_textPath);

    }
}