﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class WFCLocalPattern : MonoBehaviour
{
    public Color[,] pattern;

    public int[,][] wfcOverlapIndex;
    void Start()
    {
        pattern = new Color[2,2];
        wfcOverlapIndex = new int[3, 3][];
        //initializing local pattern with white
        for (int i = 0; i < 2; i++)
        {
            for (int k = 0; k < 2; k++)
            {
                pattern[i, k] = Color.white;
            }
        }
    }

    public void CreateOverlapIndex(List<WFCLocalPattern> localPatterns)
    {
        wfcOverlapIndex = new int[3,3][];

        //Top left(-1, -1): [0,0] of tile with [1,1] of every other tile
        int[] listTopLeft = MatchTwoCandidates(localPatterns, 0, 0, 1, 1);
        //Top right(+1, -1): [1,0] of tile with [0,1] of every other tile
        int[] listTopRight = MatchTwoCandidates(localPatterns, 1, 0, 0, 1);
        //Bottom right(+1, +1): [1,1] of tile with [0,0] of every other tile
        int[] listBottomRight = MatchTwoCandidates(localPatterns, 1, 1, 0, 0);
        //Bottom left(-1, +1): [0,1] of tile with [1,0] of every other tile
        int[] listBottomLeft = MatchTwoCandidates(localPatterns, 0, 1, 1, 0);

        //Top [0,0] [1,0] of tile with [0,1][1,1] of every other tile
        int[] listTop = MatchTwoCandidatesDual(localPatterns, 0, 0, 1, 0, 0, 1, 1, 1);
        //Right [1,0] [1,1] of tile with [0,0][0,1] of every other tile
        int[] listRight = MatchTwoCandidatesDual(localPatterns, 1, 0, 1, 1, 0, 0, 0, 1);
        //Bottom [0,1] [1,1] of tile with [0,0][1,0] of every other tile
        int[] listBottom = MatchTwoCandidatesDual(localPatterns, 0, 1, 1, 1, 0, 0, 1, 0);
        //left [0,0] [0,1] of tile with [1,0][1,1] of every other tile
        int[] listLeft = MatchTwoCandidatesDual(localPatterns, 0, 0, 0, 1, 1, 0, 1, 1);


        wfcOverlapIndex[0, 0] = listTopLeft;
        wfcOverlapIndex[1, 0] = listTop;
        wfcOverlapIndex[2, 0] = listTopRight;
        wfcOverlapIndex[0, 1] = listLeft;
        wfcOverlapIndex[0, 2] = listBottomLeft;
        wfcOverlapIndex[1, 2] = listBottom;
        wfcOverlapIndex[2, 2] = listBottomRight;
        wfcOverlapIndex[2, 1] = listRight;
    }

    public void PrintOverlapAmount() {

        StringBuilder strBuilder = new StringBuilder();

        for (int i = 0; i < 3; i++)
        {
            for (int j  = 0; j < 3; j++)
            {
                if (i == 1 && j == 1)
                {
                    strBuilder.Append("0");
                    strBuilder.Append("|");
                }
                else
                {
                    int amount = 0;

                    for (int k = 0; k < wfcOverlapIndex[j, i].Length; k++)
                    {
                        amount += wfcOverlapIndex[j, i][k];
                    }
                    strBuilder.Append(amount);
                    strBuilder.Append("|");
                }
            }
            strBuilder.Append("\n");
        }


        print(strBuilder.ToString());
    }

    int[] MatchTwoCandidates(List<WFCLocalPattern> localPatterns, int x0,int y0, int x1, int y1)
    {
        int[] matches = new int[localPatterns.Count];
        for (int i = 0; i < localPatterns.Count; i++)
        {
            if (localPatterns[i].pattern[x1, y1] == pattern[x0, y0])
            {
                matches[i] = 1;
            }
            else
                matches[i] = 0;
        }
        return matches;
    }
    int[] MatchTwoCandidatesDual(List<WFCLocalPattern> localPatterns, int x0a, int y0a, int x0b, int y0b, int x1a, int y1a, int x1b, int y1b)
    {
        int[] matches = new int[localPatterns.Count];

        for (int i = 0; i < localPatterns.Count; i++)
        {
            if (localPatterns[i].pattern[x1a, y1a] == pattern[x0a, y0a] && localPatterns[i].pattern[x1b, y1b] == pattern[x0b, y0b])
            {
                matches[i] = 1;
            }
            else
                matches[i] = 0;
        }
        return matches;
    }

    public WFCLocalPattern(int patternAmount)
    {
        pattern = new Color[2, 2];

        //initializing local pattern with white
        for (int i = 0; i < 2; i++)
        {
            for (int k = 0; k < 2; k++)
            {
                pattern[i, k] = Color.white;
            }

        }
        wfcOverlapIndex = new int[3, 3][];

        for (int i = 0; i < 3; i++)
        {
            for (int k = 0; k < 3; k++)
            {
                wfcOverlapIndex[i, k] =  new int[patternAmount];
                for (int j = 0; j < patternAmount; j++)
                {
                    wfcOverlapIndex[i, k][j] = 0;
                }
            }
        }
    }

    public WFCLocalPattern()
    {
        pattern = new Color[2, 2];

        //initializing local pattern with white
        for (int i = 0; i < 2; i++)
        {
            for (int k = 0; k < 2; k++)
            {
                pattern[i, k] = Color.white;
            }

        }
        wfcOverlapIndex = new int[3, 3][];
        //initializing local pattern with white
        for (int i = 0; i < 3; i++)
        {
            for (int k = 0; k < 3; k++)
            {
                wfcOverlapIndex[i, k] = null;
  
            }

        }
    }

    public WFCLocalPattern(Color[,] inputPattern)
    {
        pattern = new Color[2, 2];

        //initializing local pattern with white
        for (int i = 0; i < 2; i++)
        {
            for (int k = 0; k < 2; k++)
            {
                pattern[i, k] = inputPattern[i,k];
            }

        }
    }
    public void CopyPattern(Color[,] copyPattern, Color[,] replacePattern)
    {
        for (int i = 0; i < 2; i++)
        {
            for (int k = 0; k < 2; k++)
            {
                replacePattern[i, k] = copyPattern[i, k];
            }

        }
    }

    internal string PrintPattern()
    {
        StringBuilder strBuilder = new StringBuilder();

       for (int k = 0; k < 2; k++)
            {
            for (int i = 0; i < 2; i++)
            {
                strBuilder.Append(" | " + pattern[i, k].ToString());
            }
            strBuilder.Append("\n");
        }
        return strBuilder.ToString();
    }
    internal string PrintSingleColor(int x, int y)
    {
        return pattern[x, y].ToString();
    }

    internal WFCLocalPattern RotateClockwise(int count)
    {

        WFCLocalPattern srcWFCLocalPattern = new WFCLocalPattern(pattern);
        WFCLocalPattern tempWFCLocalPattern = new WFCLocalPattern(pattern);

        for (int i = 0; i < count; i++)
        {
            tempWFCLocalPattern.pattern[0, 0] = srcWFCLocalPattern.pattern[1, 0];
            tempWFCLocalPattern.pattern[1, 0] = srcWFCLocalPattern.pattern[1, 1];
            tempWFCLocalPattern.pattern[0, 1] = srcWFCLocalPattern.pattern[0, 0];
            tempWFCLocalPattern.pattern[1, 1] = srcWFCLocalPattern.pattern[0, 1];

            CopyPattern(tempWFCLocalPattern.pattern, srcWFCLocalPattern.pattern);
        }


        return tempWFCLocalPattern;
    }


    public override bool Equals(object obj)
    {
        WFCLocalPattern candidate = (WFCLocalPattern)obj;

        return
        candidate.pattern[0, 0] == pattern[0, 0] &&
        candidate.pattern[1, 0] == pattern[1, 0] &&
        candidate.pattern[0, 1] == pattern[0, 1] &&
        candidate.pattern[1, 1] == pattern[1, 1];

    }
}
