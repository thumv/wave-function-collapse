﻿using System.Collections.Generic;
using UnityEngine.Tilemaps;
using System.Text;
using UnityEngine;
using System.Collections;
using System;

public class WangTileAutomation : MonoBehaviour
{
    private int height;
    private int width;

    public Tilemap wangTileMap;
    public Tile defaultTile;

    public TileBase[] wangTiles;
    public TileBase[] grassTiles;
    public TileBase[] importTiles;
    public TileBase[] groundTiles;

    public UIController uiController;
    public TileImporter tileImporter;
    public TileEdgeDetector tileEdgeDetector;
    public bool toggleWFCAlgorithm; 
    //input tile pictures
    private TileBase[] tiles;
    private TileColorSample[] tileColorSamples;


    private int tilingSize;

    // binary: edges left top right bottom
    private readonly int[] wangCodes = {
        0b1010011011001001,
        0b1010100110101001,
        0b1100011011000110,
        0b1100100110100110,
        0b1100011010101001,
        0b1100100111001001,
        0b1010011010100110,
        0b1010100111000110
    };
    // 1010 red
    // 0110 green
    // 1100 blue
    // 1001 yellow

    private int[,] wangCodeMap;
    private WFCTile[] wfcTiles;
    private WFCTile[,] wave;
    private WFCTilePattern[] tilePattern;

    private int depth;

    private int diffTileAmount;
    private int xVal;
    private int yVal;
    private int currentIndex;
    private bool isCollapsed;
    void Start()
    {
        toggleWFCAlgorithm = true;

        diffTileAmount = GameManager.instance.tileSampleSize;

        

        tilingSize = GameManager.instance.tilingSize;
        height = GameManager.instance.tilemapHeight;
        width = GameManager.instance.tilemapWidth;
        importTiles = new TileBase[diffTileAmount];

        tiles = wangTiles;
        SetTilePaletteUI(tiles);

        xVal = 0;
        yVal = 0;
        currentIndex = UnityEngine.Random.Range(0, tiles.Length);
        isCollapsed = false;
        wave = new WFCTile[width, height];

        tileColorSamples = new TileColorSample[diffTileAmount];

        wangTileMap = this.GetComponent<Tilemap>();


       GenerateWFCTilePattern();
        GenerateTiles();
    }

    public void GenerateTiles()
    {
        StopAllCoroutines();

        if (toggleWFCAlgorithm)
            StartCoroutine(WFCTileGeneratorDelayed());
        else
            StartCoroutine(WangTileMapDelayed());
    }

    private void WFCTileGenerator()
    {

        wangTileMap.ClearAllTiles();
        height = GameManager.instance.tilemapHeight;
        width = GameManager.instance.tilemapWidth;
        wangCodeMap = new int[width, height];

        int xOffset = (width / 2);
        int yOffset = (height / 2);

        //Set TileMap Default
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                wave[i, j] = new WFCTile(tiles.Length);
                wangTileMap.SetTile((new Vector3Int(i - xOffset, j - yOffset, 0)), defaultTile);
            }
        }

        bool isFullyCollapsed = false;



        int counter = 0;

        do
        {
            ++counter;

            isFullyCollapsed = true;

            int lowestEntropyX = 0;
            int lowestEntropyY = 0;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    //look for lowest entropy (only if not collapsed):

                    if (wave[i, j].isCollapsed != true)
                    {
                        isFullyCollapsed = false;
                        if (wave[i, j].GetEntropy() < wave[lowestEntropyX, lowestEntropyY].GetEntropy())
                        {
                            lowestEntropyX = i;
                            lowestEntropyY = j;
                        }
                    }

                }
            }
            if (isFullyCollapsed)
            {
                break;
            }
 
            SetWFCTile(lowestEntropyX, lowestEntropyY);

        } while (!isFullyCollapsed && counter < width * height + 10);
    }
    private IEnumerator WFCTileGeneratorDelayed()
    {
        wangTileMap.ClearAllTiles();
        height = GameManager.instance.tilemapHeight;
        width = GameManager.instance.tilemapWidth;
        wangCodeMap = new int[width, height];

        wave = new WFCTile[width, height];


        int xOffset = (width / 2);
        int yOffset = (height / 2);

        bool isDelayed = GameManager.instance.isDelayed;
        float delay = GameManager.instance.GetDelay();

        //Set TileMap Default
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                wave[i, j] = new WFCTile(tiles.Length);

                wangTileMap.SetTile((new Vector3Int(i - xOffset, j - yOffset, 0)), defaultTile);
            }
        }

        bool isFullyCollapsed = false;



        int counter = 0;

        do {
            ++counter;

            isFullyCollapsed = true;

            int lowestEntropyX = 0;
            int lowestEntropyY = 0;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    //look for lowest entropy (only if not collapsed):

                    if (wave[i, j].isCollapsed != true)
                    {
                        isFullyCollapsed = false;
                        if (wave[i, j].GetEntropy() < wave[lowestEntropyX, lowestEntropyY].GetEntropy() || wave[lowestEntropyX,lowestEntropyY].GetEntropy() == 0 && wave[i, j].GetEntropy() > 0)
                        {
                            lowestEntropyX = i;
                            lowestEntropyY = j;
                        }
                    }
                
                }
            }
            if (isFullyCollapsed)
            {
                print("wfc - is fully collapsed");
                break;
            }
            // -> Continue Collapse with coordinates
            if(isDelayed)
                yield return new WaitForSeconds(delay);

            SetWFCTile(lowestEntropyX, lowestEntropyY);

        } while (!isFullyCollapsed && counter < width * height + 10);

    }    

    private void SetWFCTile(int lowestEntropyX, int lowestEntropyY)
    {

        int xOffset = (width / 2);
        int yOffset = (height / 2);

        //get random candidate:
        int candidate = wave[lowestEntropyX, lowestEntropyY].GetPossible();
        if (candidate >= 0 && tiles.Length > 0)
        {
            wave[lowestEntropyX, lowestEntropyY].SetCollapsed(candidate);
            wangTileMap.SetTile((new Vector3Int(lowestEntropyX - xOffset, lowestEntropyY - yOffset, 0)), tiles[candidate]);

            //iterate through tile sprites:
            //uiController.SetImportImagePreview(((Tile)tiles[candidate]).sprite);
            PropagateOnWFCTileMap(lowestEntropyX, lowestEntropyY);
        }
        else print("wfc - no candidate found");
    }

    private void PropagateOnWFCTileMap(int coordinateX, int coordinateY)
    {
        depth = 0;

        PrintWaveEntropy(wave, width, height);


        if (wave[coordinateX, coordinateY].isCollapsed)
        {
            ResetPropagationStatus();
            
            if (wave[coordinateX, coordinateY].hasBeenVisited) return;
            //LEFT
            int x_shift = -1;
            int y_shift = 0;

            wave[coordinateX, coordinateY].hasBeenVisited = true;

            if (coordinateX + x_shift >= 0 && !wave[coordinateX + x_shift, coordinateY + y_shift].hasBeenVisited && !wave[coordinateX + x_shift, coordinateY + y_shift].isCollapsed)
            {
                CompareNeighbour(coordinateX, coordinateY, x_shift, y_shift);

               //PropageOnNeighboursRecursive(coordinateX + x_shift, coordinateY + y_shift);
            }
            //RIGHT
            x_shift = 1;
            y_shift = 0;
            if (coordinateX + x_shift < width && !wave[coordinateX + x_shift, coordinateY + y_shift].hasBeenVisited && !wave[coordinateX + x_shift, coordinateY + y_shift].isCollapsed)
            {
                CompareNeighbour(coordinateX, coordinateY, x_shift, y_shift);

                //PropageOnNeighboursRecursive(coordinateX + x_shift, coordinateY + y_shift);
            }
            //BOTTOM
            x_shift = 0;
            y_shift = -1;
            if (coordinateY + y_shift >= 0 && !wave[coordinateX + x_shift, coordinateY + y_shift].hasBeenVisited && !wave[coordinateX + x_shift, coordinateY + y_shift].isCollapsed)
            {
                CompareNeighbour(coordinateX, coordinateY, x_shift, y_shift);

              //PropageOnNeighboursRecursive(coordinateX + x_shift, coordinateY + y_shift);
            }
            //TOP
            x_shift = 0;
            y_shift = 1;
            if (coordinateY + y_shift < height && !wave[coordinateX + x_shift, coordinateY + y_shift].hasBeenVisited && !wave[coordinateX + x_shift, coordinateY + 1].isCollapsed)
            {
                CompareNeighbour(coordinateX, coordinateY, x_shift, y_shift);

              //PropageOnNeighboursRecursive(coordinateX + x_shift, coordinateY + y_shift);
            }
            return;                  
        }
       
    }

    private void ResetPropagationStatus()
    {
        for (int i = 0; i < width; i++)
        {
            for (int l = 0; l < height; l++)
            {
                wave[i, l].hasBeenVisited = false;
            }
            
        }
    }

    public void PropageOnNeighboursRecursive(int coordinateX, int coordinateY)
    {
        

        //(REKURSIVANKER) wenn tile besucht, beende methode:
        if (wave[coordinateX, coordinateY].hasBeenVisited) return;

        wave[coordinateX, coordinateY].hasBeenVisited = true;

        //If not visited, visit neighbours
        //LEFT
        int x_shift =  - 1;
        int y_shift = 0;

        if (coordinateX + x_shift >= 0 )
        {
            // print("left");

            CompareNeighbour(coordinateX, coordinateY, x_shift, y_shift);
            if (!(wave[coordinateX + x_shift, coordinateY + y_shift].GetEntropy() <= 0) && !wave[coordinateX + x_shift, coordinateY + y_shift].hasBeenVisited && !wave[coordinateX + x_shift, coordinateY + y_shift].isCollapsed)
                PropageOnNeighboursRecursive(coordinateX + x_shift, coordinateY + y_shift);
        }
        //RIGHT
        x_shift = 1;
        y_shift = 0;
        if (coordinateX + x_shift < width )
        {
            //print("right");
            //print("coordx + i: " + (coordinateX + 1) + "| coordy + j: " + (coordinateY));
            CompareNeighbour(coordinateX, coordinateY, x_shift, y_shift);
            if (!(wave[coordinateX + x_shift, coordinateY + y_shift].GetEntropy() <= 0) && !wave[coordinateX + x_shift, coordinateY + y_shift].hasBeenVisited && !wave[coordinateX + x_shift, coordinateY + y_shift].isCollapsed)
                PropageOnNeighboursRecursive(coordinateX + x_shift, coordinateY + y_shift);
        }
        //BOTTOM
        x_shift = 0;
        y_shift = -1;
        if (coordinateY + y_shift >= 0 )
        {
           //print("bottom");
            CompareNeighbour(coordinateX, coordinateY, x_shift, y_shift);

            if (!(wave[coordinateX + x_shift, coordinateY + y_shift].GetEntropy() <= 0) && !wave[coordinateX + x_shift, coordinateY + y_shift].hasBeenVisited && !wave[coordinateX + x_shift, coordinateY + y_shift].isCollapsed)
                PropageOnNeighboursRecursive(coordinateX + x_shift, coordinateY + y_shift);
        }
        //TOP
        x_shift = 0;
        y_shift = 1;
        if (coordinateY + y_shift < height)
        {
           // print("top"); 
            CompareNeighbour(coordinateX, coordinateY, x_shift, y_shift);

            if (!(wave[coordinateX + x_shift, coordinateY + y_shift].GetEntropy() <= 0) && !wave[coordinateX + x_shift, coordinateY + y_shift].hasBeenVisited && !wave[coordinateX + x_shift, coordinateY + 1].isCollapsed)
                PropageOnNeighboursRecursive(coordinateX + x_shift, coordinateY + y_shift);
        }
    }

    private void CompareNeighbour(int coordinateX, int coordinateY, int x_shift, int y_shift)
    {
        int value;
        if (wave[coordinateX, coordinateY].isCollapsed)
            for (int k = 0; k < tiles.Length; k++)
            {
                value = tilePattern[wave[coordinateX, coordinateY].tileValue].pattern[x_shift + 1, y_shift + 1].possible_tiles[k];

                wave[coordinateX + x_shift, coordinateY + y_shift].possible_tiles[k] = wave[coordinateX + x_shift, coordinateY + y_shift].possible_tiles[k] & value;   
            } 
        else
        {
            //sum of tilePattern[k]
            WFCTile tileSum = new WFCTile(tiles.Length);
            for (int k = 0; k < tiles.Length; k++)
            {
                for (int i = 0; i < tiles.Length; i++)
                {
                    tileSum.possible_tiles[i] |= tilePattern[k].pattern[x_shift + 1, y_shift + 1].possible_tiles[i];
                }
                wave[coordinateX + x_shift, coordinateY + y_shift].possible_tiles[k] &=  tileSum.possible_tiles[k];
            }
        }
    }

    private void PrintWaveEntropy(WFCTile[,] wave, int _width, int _height)
    {
        StringBuilder text = new StringBuilder();

        for (int i = 0; i < _width; i++)
        {
            for (int j = 0; j < _height; j++)
            {
                text.Append(wave[i, j].GetEntropy());
                if (wave[i, j].GetEntropy() == 1 || wave[i, j].GetEntropy() == 0) text.Append(",0 ");
                else text.Append(" ");

            }
            text.Append("\n");
        }

        print(text.ToString());

    }
    private void GenerateWFCTilePattern()
    {
        tilePattern = new WFCTilePattern[tiles.Length];
        wfcTiles = new WFCTile[tiles.Length];


        for (int i = 0; i < tiles.Length; i++)
        {

            //Set wfc tile with collapsed tile in the middle 
            wfcTiles[i] = new WFCTile(tiles.Length);
            wfcTiles[i].SetCollapsed(i);

            //sourrounded by "pattern" of four touching tiles:
            tilePattern[i] = new WFCTilePattern();
            tilePattern[i].SetPatternForTile(wfcTiles[i]);
        } 

    }


    //WONG TILES DELAYED:
    private IEnumerator WangTileMapDelayed()
    {
        print("start wong tile generator coroutine");
        wangTileMap.ClearAllTiles();
        height = GameManager.instance.tilemapHeight;
        width = GameManager.instance.tilemapWidth;
        float delay = GameManager.instance.GetDelay();
        bool isDelayed = GameManager.instance.isDelayed;

        wangCodeMap = new int[width, height];

        print("find in tiles (Count:" + tiles.Length + ")");
        currentIndex = UnityEngine.Random.Range(0, tiles.Length);

        int xOffset = (width / 2);
        int yOffset = (height / 2);

        wangTileMap.SetTile((new Vector3Int(-xOffset, -yOffset, 0)), tiles[currentIndex]);
        wangCodeMap[0, 0] = currentIndex;

        //Edge Detection:
        tileColorSamples = tileEdgeDetector.DetectEdgeColors(tiles);



        for (int i = 1; i < width; i++)
        {

            //left match partner:

            int matchNum = tileColorSamples[wangCodeMap[i - 1, 0]].RandomMatchRight(tileColorSamples);
            if (matchNum < 0)
            {
                print("no match found");
                continue;
            }
            if (isDelayed) 
                yield return new WaitForSeconds(delay);

            wangTileMap.SetTile((new Vector3Int(i - xOffset, -yOffset, 0)), tiles[matchNum]);
            wangCodeMap[i, 0] = matchNum;

        } //untere reihe voll


        for (int j = 1; j < height; j++)
        {            
            int matchNum = tileColorSamples[wangCodeMap[0, j - 1]].RandomMatchTop(tileColorSamples);
            if (matchNum < 0)
            {
    
                continue;
            }
            if (isDelayed)
                yield return new WaitForSeconds(delay);

            wangTileMap.SetTile((new Vector3Int(-xOffset, j - yOffset, 0)), tiles[matchNum]);

            wangCodeMap[0, j] = matchNum;
        } //linke spalte voll
        
        for (int i = 1; i < width; i++)
        {
            for (int j = 1; j < height; j++)
            {             
                int matchNum = tileColorSamples[wangCodeMap[i, j ]].RandomMatchLeftBottom(tileColorSamples[wangCodeMap[i - 1, j]], tileColorSamples[wangCodeMap[i, j - 1]], tileColorSamples);
              
                if (matchNum < 0)
                {
                    continue;
                }
              
                if (isDelayed) 
                    yield return new WaitForSeconds(delay);

                wangTileMap.SetTile((new Vector3Int(i - xOffset, j - yOffset, 0)), tiles[matchNum]);
                wangCodeMap[i, j] = matchNum;
            }
        }
        
    }
  

    void Update()
    {     
        if (Input.GetKeyDown(KeyCode.R))
            GenerateTiles();
    }


    private void FindLowestEntropy()
    {
        wave[xVal, yVal].SetAllValuesZero(currentIndex);

        int lowestx = 0;
        int lowesty = 0;


        //set starting value for entropy
        if (xVal > 0)
        {
            lowestx = xVal - 1;
        }
        else if (xVal < width)
        {
            lowestx = xVal + 1;
        }

        if (yVal > 0)
        {
            lowesty = yVal - 1;
        }
        else if (yVal < height)
        {
            lowesty = yVal + 1;
        }

 
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (wave[i, j].GetEntropy() != 0)
                    isCollapsed = false;
                else
                    continue;

                if (wave[i, j].GetEntropy() < wave[lowestx, lowesty].GetEntropy())
                {
                    lowestx = i;
                    lowesty = j;
                }
            }
        }
        //Kandidat mit niedrigster Entropie lowestx lowesty
    }


    public void SetToWangTiles()
    {
        tiles = wangTiles;
        SetTilePaletteUI(tiles);
    }

    public void SetToGrassTiles()
    {
        tiles = grassTiles;
        SetTilePaletteUI(tiles);
    }

    public void SetToImportTiles(string importPath)
    {
        tiles = tileImporter.LoadAndSliceImportImage(importPath);
        if (tiles == null)
            SetToWangTiles();
        else
            GenerateWFCTilePattern();
    }

    public void SetTilePaletteUI(TileBase[] tiles)
    {
        uiController.SetTilePaletteUI(tiles);
    }

    public TileBase[] GetTiles()
    {
        return tiles;
    }
}
