﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WFCTilePattern : MonoBehaviour
{

    public WFCTile[,] pattern;
    private TileEdgeDetector tileEdgeDetector;
    private TileColorSample[] tileColorSamples;
    private WangTileAutomation wangTileAutomation;

    // binary: edges left top right bottom
    private readonly int[] wangCodes = {
        0b1010011011001001,
        0b1010100110101001,
        0b1100011011000110,
        0b1100100110100110,
        0b1100011010101001,
        0b1100100111001001,
        0b1010011010100110,
        0b1010100111000110
    };
    void Start()
    {
        tileEdgeDetector = GameManager.instance.tileEdgeDetector;
        wangTileAutomation = GameManager.instance.wangTileGenerator;
        pattern = new WFCTile[3, 3];
    }

    public WFCTilePattern()
    {
        pattern = new WFCTile[3, 3];
    }
   
    public void SetPatternForTile(WFCTile inputTile)
    {
        //Get actual tiles for sampling:   
        tileEdgeDetector = GameManager.instance.tileEdgeDetector;
        wangTileAutomation = GameManager.instance.wangTileGenerator;

        tileColorSamples = tileEdgeDetector.DetectEdgeColors(wangTileAutomation.GetTiles());
        int colorSampleAmount = tileColorSamples.Length;

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (i == 1 && j == 1)
                    pattern[1, 1] = inputTile;
                else
                    pattern[i, j] = new WFCTile(inputTile.possible_tiles.Length);
            }
        }


        for (int i = 0; i < colorSampleAmount; i++)
        {
            //match left tile with middle
            if (tileColorSamples[inputTile.tileValue].MatchLeft(tileColorSamples[i]))
            {
                pattern[0, 1].SetValue(i, 1);
            }
            else pattern[0, 1].SetValue(i, 0);

            //match right tile with middle
            if (tileColorSamples[inputTile.tileValue].MatchRight(tileColorSamples[i]))
            {
                pattern[2, 1].SetValue(i, 1);
            }
            else pattern[2, 1].SetValue(i, 0);

            //match top tile with middle
            if (tileColorSamples[inputTile.tileValue].MatchTop(tileColorSamples[i]))
            {
                pattern[1, 2].SetValue(i, 1);
            }
            else pattern[1, 2].SetValue(i, 0);

            //match bottom tile with middle
            if (tileColorSamples[inputTile.tileValue].MatchBottom(tileColorSamples[i]))
            {
                pattern[1, 0].SetValue(i, 1);
            }
            else pattern[1, 0].SetValue(i, 0);


        }
    }
}
