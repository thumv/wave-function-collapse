﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class WFCOverlapController : MonoBehaviour
{
    public Image importedImage;
    public Image generatorImage;
    public UIController uiController;
    public bool isRotationAllowed;

    private List<WFCLocalPattern> m_localPatterns;

    private WFCEntity[,] wave;
    private WFCEntity[,] stamp;

 
    private int m_width;
    private int m_height;
    private bool isCollapsed;

    private Texture2D m_sampleTexture;
    // Start is called before the first frame update
    void Start()
    {
        isCollapsed = false;
        isRotationAllowed = true;

        m_localPatterns = new List<WFCLocalPattern>();
        m_width = GameManager.instance.GetWidth();
        m_height = GameManager.instance.GetHeight();
    }

    public void GenerateTexture()
    {
        if (importedImage?.sprite == null || m_localPatterns.Count < 1)
        {
            print("not ready to generate");
            return;
        }
        StopAllCoroutines();
        StartCoroutine(GenerateTextureAndWait());
    }

    public IEnumerator GenerateTextureAndWait()
    {
        //initialize wave:
        m_width = GameManager.instance.GetWidth();
        m_height = GameManager.instance.GetHeight();
        int possibilities = m_localPatterns.Count;
        wave = new WFCEntity[m_width, m_height];



        for (int i = 0; i < m_width; i++)
        {
            for (int j = 0; j < m_height; j++)
            {
                wave[i, j] = new WFCEntity(possibilities);
                for (int k = 0; k < possibilities; k++)
                {
                    wave[i, j].possibilites[k] = 1;
                }
            }
        }
        yield return new WaitForSeconds(0.01f);

        //set initial pixel set of 2x2
        //wave[x_start, y_start] = m_localPatterns[0].pattern[0, 0];

        Color[] block = new Color[4];
        block[0] = m_localPatterns[0].pattern[0, 1];
        block[1] = m_localPatterns[0].pattern[1, 1];
        block[2] = m_localPatterns[0].pattern[0, 0];
        block[3] = m_localPatterns[0].pattern[1, 0];


        int x_start = m_width / 2;
        int y_start = (m_height / 2);


        // Punkt aus X-Position und Y-Position ist Mittelpunkt einer 2x2-Kachel!

        Texture2D generatorTex = new Texture2D(m_width, m_height);
        generatorTex.filterMode = FilterMode.Point;

        int startcandidate = UnityEngine.Random.Range(0, m_localPatterns.Count - 1);

        PropagatePattern(x_start, y_start, startcandidate);

        //setzen der pixel in texture immer mit verdrehter y-achse!!!
        //Start des blocks versetzt nach oben links (height - 1  = tatsächliches max. wert in y-richtung):
        generatorTex.SetPixels(x_start - 1, m_height - 1 - y_start - 1, 2, 2, block, 0);
        generatorTex.Apply();

        generatorImage.sprite = Sprite.Create(generatorTex, new Rect(0, 0, m_width, m_height), new Vector2(0, 0));



        //main loop
        int counter = 0;
        isCollapsed = false;
        while (!isCollapsed && counter < m_width * m_height + 100)
        {
            //STEP 1: find lowest entropy
            (int lowEntr_x, int lowEntr_y) = GetLowestEntropy();

            //STEP 2: set overlap 2x2 pixels
            //OBSERVE
            int candidate = GetRandomPossibility(lowEntr_x, lowEntr_y);


            //STEP 3: propagate on wave 
            PropagatePattern(lowEntr_x, lowEntr_y, candidate);

            block[0] = m_localPatterns[candidate].pattern[0, 1];
            block[1] = m_localPatterns[candidate].pattern[1, 1];
            block[2] = m_localPatterns[candidate].pattern[0, 0];
            block[3] = m_localPatterns[candidate].pattern[1, 0];

            //setzen der pixel in texture immer mit verdrehter y-achse!!!
            //Start des blocks versetzt nach oben links (height - 1  = tatsächliches max. wert in y-richtung):

            if ((lowEntr_x - 1 < 0) || (lowEntr_y < 0) || (lowEntr_x > m_width - 1) || (lowEntr_y > m_height - 2)) continue;

            generatorTex.SetPixels(lowEntr_x - 1, m_height - 1 - lowEntr_y - 1, 2, 2, block, 0);
            generatorTex.Apply();
           
            generatorImage.sprite = Sprite.Create(generatorTex, new Rect(0, 0, m_width, m_height), new Vector2(0, 0));

            yield return new WaitForSeconds(0.01f);
            counter++;
            if (isCollapsed) {
                uiController.DisplayMessage("wave collapsed!");
                print("wave collapsed!");

            }
            //STEP 4: if not collapsed JUMP to step 1
        }
    }



    private int GetRandomPossibility(int x_pos, int y_pos)
    {
        List<int> canditates = new List<int>();

        for (int i = 0; i < wave[x_pos, y_pos].possibilites.Length; i++)
        {
            if (wave[x_pos, y_pos].possibilites[i] == 1)
            {
                canditates.Add(i);
            }
        }
        if (canditates.Count > 0)
        {

            int randomAmount = UnityEngine.Random.Range(0, canditates.Count);

            return canditates[randomAmount];

        }
        else
            return 0;
    }

    private (int, int) GetLowestEntropy()
    {
        isCollapsed = true;
        float lowestEntropyValue = 1.0f;
        (int, int) lowestEntropyPos = (0, 0);

        for (int i = 0; i < m_width; i++)
        {
            for (int j = 0; j < m_height; j++)
            {
                if (!wave[i, j].isCollapsed) isCollapsed = false;
                //else wave[i, j].Clear();
                if (!wave[i, j].isCollapsed && wave[i, j].GetAmount() > 0 && lowestEntropyValue > wave[i, j].GetEntropy())
                {
                    lowestEntropyPos = (i, j);
                    lowestEntropyValue = wave[i, j].GetEntropy();
                }


            }

        }
        return lowestEntropyPos;
    }


    private void PropagatePattern(int x_pos, int y_pos, int v)
    {
        int possibilites = m_localPatterns.Count;

        wave[x_pos, y_pos].isCollapsed = true;
        wave[x_pos, y_pos].needsToBeUpdated = false;


        int x_min = x_pos - 1;
        int y_min = y_pos - 1;
        int x_max = x_pos + 1;
        int y_max = y_pos + 1;

        for (int i = 0; i < 3; i++)
            if(x_min + i >= 0 && x_min + i < m_width)
                for (int j = 0; j < 3; j++)
                {
                    if (i == 1 && j == 1) continue;
                    if (y_min + j >= 0 && y_min + j < m_height) {

                        bool isEqualLocally = true;

                        for (int k = 0; k < possibilites; k++)
                        {
                            if (wave[x_min + i, y_min + j].possibilites[k] == 1 && m_localPatterns[v].wfcOverlapIndex[i, j][k] == 0)
                            {
                                isEqualLocally = false;
                                
                            }
                            wave[x_min + i, y_min + j].possibilites[k] &= m_localPatterns[v].wfcOverlapIndex[i, j][k];
                            //if != set flagged: needs to be updated in next iteration:


                        }
                        // if collapsed there is no need for updating anything
                        if(wave[x_min + i, y_min + j].isCollapsed) wave[x_min + i, y_min + j].needsToBeUpdated = false;
                        else wave[x_min + i, y_min + j].needsToBeUpdated = !isEqualLocally;

                        if (wave[x_min + i, y_min + j].needsToBeUpdated)
                        {
                            WFCEntity wfcEntitySet = new WFCEntity(possibilites, 0);

                            for (int k = 0; k < possibilites; k++)
                            {
                                wfcEntitySet.possibilites[k] |= wave[x_min + i, y_min + j].possibilites[k];
                            }

                            if (wfcEntitySet.GetAmount() < possibilites && wfcEntitySet.GetAmount() > 1)
                                PropagatePattern(x_min + i, y_min + j, wfcEntitySet);
                        }

                    }


                }
        PrintWave();
    }

    private void PropagatePattern(int x_pos, int y_pos, WFCEntity set)
    {

        if (!wave[x_pos, y_pos].needsToBeUpdated)
            return;

        int possibilites = m_localPatterns.Count;

        wave[x_pos, y_pos].needsToBeUpdated = false;

        int x_min = x_pos - 1;
        int y_min = y_pos - 1;
        int x_max = x_pos + 1;
        int y_max = y_pos + 1;

        //combine overlap indices
        WFCLocalPattern combinedWFCOverlapSet = new WFCLocalPattern(possibilites);
        for (int l = 0; l < possibilites; l++)
        {
            if (set.possibilites[l] == 1)
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (i == 1 && j == 1) continue;
                        //combine all possible neibours of a possibility entity in the wave
                        for (int k = 0; k < possibilites; k++)
                        {                
                                combinedWFCOverlapSet.wfcOverlapIndex[i, j][k] |= m_localPatterns[l].wfcOverlapIndex[i, j][k];
                        }
                    }

                }
        }

        for (int i = 0; i < 3; i++)
            if (x_min + i >= 0 && x_min + i < m_width)
                for (int j = 0; j < 3; j++)
                {
                    if (i == 1 && j == 1) continue;
                    if (y_min + j >= 0 && y_min + j < m_height)
                    {
                        bool isEqualLocally = true;

                        for (int k = 0; k < possibilites; k++)
                        {
                            if (wave[x_min + i, y_min + j].possibilites[k] == 1 && combinedWFCOverlapSet.wfcOverlapIndex[i, j][k] == 0)
                            {
                                isEqualLocally = false;
                            }
                            wave[x_min + i, y_min + j].possibilites[k] &= combinedWFCOverlapSet.wfcOverlapIndex[i, j][k];
                        }

                        // if collapsed there is no need for updating anything
                        if (wave[x_min + i, y_min + j].isCollapsed) wave[x_min + i, y_min + j].needsToBeUpdated = false;
                        else wave[x_min + i, y_min + j].needsToBeUpdated = !isEqualLocally;                        

                        if (wave[x_min + i, y_min + j].needsToBeUpdated)
                        {
                            WFCEntity wfcEntitySet = new WFCEntity(possibilites, 0);

                            for (int k = 0; k < possibilites; k++)
                            {

                                    wfcEntitySet.possibilites[k] |= wave[x_min + i, y_min + j].possibilites[k];
                            }
                             if (wfcEntitySet.GetAmount() < possibilites && wfcEntitySet.GetAmount() > 1)
                                PropagatePattern(x_min + i, y_min + j, wfcEntitySet);
                        }
                    }
                }
    }
    public void PatternsFromImportedImageTwoByTwo()
    {
        if (importedImage?.sprite == null)
        {
            uiController.DisplayMessage("No image found!");
            print("image null");
            return;
        }

        if (m_sampleTexture?.width > 0)
        {
            print("source tex height: " + m_sampleTexture.height + "| source tex width: " + m_sampleTexture.width);

            //extract local patterns:
            m_localPatterns.Clear();

            int y = m_sampleTexture.height;
            for (int j = 1; j < y; j++)
            {
                for (int i = 0; i < m_sampleTexture.width - 1; i++)
                {
                    WFCLocalPattern tmpPattern = new WFCLocalPattern();
                    tmpPattern.pattern[0, 0] = m_sampleTexture.GetPixel(i, y - j);
                    tmpPattern.pattern[1, 0] = m_sampleTexture.GetPixel(i + 1, y - j);
                    tmpPattern.pattern[0, 1] = m_sampleTexture.GetPixel(i, y - j - 1);
                    tmpPattern.pattern[1, 1] = m_sampleTexture.GetPixel(i + 1, y - j - 1);

                    if (!CompareCandidateToList(m_localPatterns, tmpPattern))
                        m_localPatterns.Add(tmpPattern);
                }
            }

            print("source pattern amount: " + m_localPatterns.Count);
            if (isRotationAllowed)
            {
                //REFLECTION AND ROTATION
                //rotate 3 times and add each step (ONLY in N=2)  
                int sourcePatternAmount = m_localPatterns.Count;
                for (int i = 0; i < sourcePatternAmount; i++)
                {
                    WFCLocalPattern tempPattern01 = m_localPatterns[i].RotateClockwise(1);

                    WFCLocalPattern tempPattern02 = m_localPatterns[i].RotateClockwise(2);

                    WFCLocalPattern tempPattern03 = m_localPatterns[i].RotateClockwise(3);

                    if (!CompareCandidateToList(m_localPatterns, tempPattern01))
                        m_localPatterns.Add(tempPattern01);
                    if (!CompareCandidateToList(m_localPatterns, tempPattern02))
                        m_localPatterns.Add(tempPattern02);
                    if (!CompareCandidateToList(m_localPatterns, tempPattern03))
                        m_localPatterns.Add(tempPattern03);
                }

                print("pattern amount after rotation : " + m_localPatterns.Count);
            }

            uiController.DisplayMessage(m_localPatterns.Count + " overlapping pattern built!");

            for (int i = 0; i < m_localPatterns.Count; i++)
            {
                m_localPatterns[i].CreateOverlapIndex(m_localPatterns);
            }
        }

    }
    public void PrintWave()
    {
        StringBuilder strBuilder = new StringBuilder();
        for (int j = 0; j < m_height; j++)
            {
            for (int i = 0; i < m_width; i++)
            {  
                float entropy = wave[i, j].GetEntropy();
                if (entropy == 1 | entropy == 0)
                    strBuilder.Append(" ");
                entropy *= 10;
                int entropyInt = (int) entropy;

                entropy = (float) entropyInt / 10;
 
                strBuilder.Append(entropy);
                if (entropy == 1 | entropy == 0)
                    strBuilder.Append("  ");
                strBuilder.Append("|");

            }
            strBuilder.AppendLine();
        }
        print(strBuilder.ToString());
    }

    public void PrintUpdateWave()
    {
        StringBuilder strBuilder = new StringBuilder();
        for (int j = 0; j < m_height; j++)
        {
            for (int i = 0; i < m_width; i++)
            {   bool type = wave[i,  j].needsToBeUpdated;
                strBuilder.Append("  ");
                if(type)
                    strBuilder.Append("1");
                else
                    strBuilder.Append("0");
                strBuilder.Append("|");
            }
            strBuilder.AppendLine();
        }
        print(strBuilder.ToString());
    }
  
    
    public void PrintPatterns()
    {

        for (int i = 0; i < m_localPatterns.Count; i++)
        {
            print(m_localPatterns[i].PrintPattern());
            print("............................................................");
        }

    }
    public bool CompareCandidateToList(List<WFCLocalPattern> localPatterns, WFCLocalPattern candidate)
    {
        for (int i = 0; i < localPatterns.Count; i++)
        {
                if (localPatterns[i].Equals(candidate))
                    return true;
        }
        return false;
    }
    internal void SetImportImage(Texture2D sampleTexture)
    {
        if(sampleTexture != null)
            m_sampleTexture = sampleTexture;
    }

    public void SetRotation(bool value)
    {
        isRotationAllowed = value;

    }
}
