﻿using UnityEngine.UI;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public TextFileFinder fileFinder;
    public WangTileAutomation wangTileGenerator;
    public UIController uiController;
    public TileImporter tileImporter;
    public TileEdgeDetector tileEdgeDetector;

    public int tilingSize;
    public int tilemapWidth;
    public int tilemapHeight;
    public int tileSampleSize = 8;
    public float thresholdValue = 0.2f;
    public float delay;
    public bool isDelayed;

    public InputField heightInput;
    public InputField widthInput;
    public InputField delayInput;

    public bool isOverlapModel;
    public int maximumTilemapSize = 100;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        isOverlapModel = true;
        heightInput.text = tilemapHeight.ToString();
        widthInput.text = tilemapWidth.ToString();
        delayInput.text = delay.ToString();
        heightInput.interactable = true;
        widthInput.interactable = true;
        delayInput.interactable = true;
        isDelayed = true;
    }


    public void GenerateWangTiles()
    {
        wangTileGenerator.SetToWangTiles();
        wangTileGenerator.GenerateTiles();
    }

    public void GenerateGrassTiles()
    {
        wangTileGenerator.SetToGrassTiles();
        wangTileGenerator.GenerateTiles();
    }

    public void GenerateFromImport()
    {
        wangTileGenerator.SetToImportTiles(fileFinder.m_textPath);
        wangTileGenerator.GenerateTiles();
    }

    internal void InvokeUIImportImagePreviewLoading(String m_textPath)
    {
        if (m_textPath == null) return;

        if (!GameManager.instance.isOverlapModel)
        {
            tileImporter.LoadImportImage(m_textPath);
            
        }
        else
            tileImporter.LoadImportImage(m_textPath);
    }

    public void SetTileMapHeight()
    {
        int numValue = int.Parse(heightInput.text);
        if (numValue <= maximumTilemapSize && numValue > 0)
            tilemapHeight = numValue;
        else
            uiController.DisplayMessage("Tile map height not accepted!");

    }
    public int GetHeight()
    {
        return int.Parse(heightInput.text);
    }
    public int GetWidth()
    {
        return int.Parse(widthInput.text);
    }

    public void SetTileMapWidth()
    {
        int numValue = int.Parse(widthInput.text);
        if (numValue <= maximumTilemapSize && numValue > 0)
            tilemapWidth = numValue;
        else
            uiController.DisplayMessage("Tile map width not accepted!");
    }
    public void SetDelay()
    {    
        delay = float.Parse(delayInput.text.ToString());
        print("delay: " + delay);
    }

    public void ToggleDelay()
    {
        isDelayed = !isDelayed;
    }

    public float GetDelay()
    {
        return delay;
    }

    internal void InvokeUIPaletteLoading()
    {
        wangTileGenerator.SetToImportTiles(fileFinder.m_textPath);
    }

    public void SetWFC()
    {
        wangTileGenerator.toggleWFCAlgorithm = true;
    }
    public void SetWONG()
    {
        wangTileGenerator.toggleWFCAlgorithm = false;
    }

    public void LoadTilingModel()
    {
        SceneManager.LoadScene(0);
    }
    public void LoadOverlapModel()
    {
        SceneManager.LoadScene(1);
    }
}
