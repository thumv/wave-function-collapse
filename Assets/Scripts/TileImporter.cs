﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Tilemaps;



public class TileImporter : MonoBehaviour
{
    public UIController uiController;
    public TileEdgeDetector tileEdgeDetector;
    public WFCOverlapController wfcOverlapController;
    private int diffTileAmount;
    private int tilingSize;

    private TileBase[] eightImportTiles;
    public List<TileBase> importedTiles;

    private bool isNotOneByEight;
    void Start() {
        isNotOneByEight = false;
        importedTiles = new List<TileBase>();

        diffTileAmount = GameManager.instance.tileSampleSize;
        tilingSize = GameManager.instance.tilingSize;
     
        eightImportTiles = new TileBase[diffTileAmount];

    }

    internal void LoadImportImage(string m_textPath)
    {
        try
        {

            byte[] byteArray = File.ReadAllBytes(@m_textPath);
            Texture2D sampleTexture = new Texture2D(2, 2)
            {
                filterMode = FilterMode.Point //ohne diesem expliziten befehl: spalten zwischen tiles
            };
            bool isLoaded = sampleTexture.LoadImage(byteArray);//..this will auto-resize the texture dimensions.

            if (isLoaded)
            {

                Sprite tmpSprite = Sprite.Create(sampleTexture, new Rect(0, 0, sampleTexture.width, sampleTexture.height), new Vector2(.0f, .0f));
                uiController.SetImportImagePreview(tmpSprite);
                wfcOverlapController.SetImportImage(sampleTexture);
            }
            else
            {
                uiController.DisplayMessage("image import load failed in step 2");
                print("STEP 2: img load failed");
            }
        }
        catch (Exception)
        {
            uiController.DisplayMessage("exception; catch: image import failed in step 1");
            print("STEP 1: img load failed");

        }
    }

    internal TileBase[] LoadImportTilesOneByEight(string importPath)
    {
        TileBase[] tiles;
        Vector2[] tileSourcePositions = new Vector2[8];
        try
        {
            byte[] byteArray = File.ReadAllBytes(@importPath);
            Texture2D sampleTexture = new Texture2D(2, 2)
            {
                filterMode = FilterMode.Point //ohne diesem expliziten befehl: spalten zwischen tiles
            };
            bool isLoaded = sampleTexture.LoadImage(byteArray);//..this will auto-resize the texture dimensions.

            if (isLoaded)
            {
                int offset = 0;

                for (int i = 0; i < eightImportTiles.Length; i++)
                {
                    int x = 0;
                    int y = offset;

                    Sprite tmpSprite = Sprite.Create(sampleTexture, new Rect(0, y, tilingSize, tilingSize), new Vector2(.0f, .0f), tilingSize, 1, SpriteMeshType.FullRect);
                    tileSourcePositions[i] = new Vector2(x, y);


                    //tileEdgeDetector.DetectEdgeColors(tmpSprite);
                    var tile = new Tile
                    {
                        sprite = tmpSprite
                    };

                    eightImportTiles[i] = tile;

                    offset += tilingSize;
                }

                tiles = eightImportTiles;
                uiController.SetTilePaletteUI(tiles);
            }
            else
            {
                uiController.DisplayMessage("1 By 8 Tileset import fail(Step 2). set to wong tiles");
                return null;
            }
        }
        catch (System.Exception)
        {
            uiController.DisplayMessage("1 By 8 Tileset import fail (Step 1). set to wong tiles");
            return null;
        }
        return tiles;
    }

    internal TileBase[] LoadAndSliceImportImage(string importPath)
    {
        List<TileBase> tileList = new List<TileBase>();
        TileBase[] tiles;
        float threshholdValue = GameManager.instance.thresholdValue;

        
            byte[] byteArray = File.ReadAllBytes(@importPath);
            Texture2D sampleTexture = new Texture2D(2, 2)
            {
                filterMode = FilterMode.Point //ohne diesem expliziten befehl: spalten zwischen tiles
            };
            bool isLoaded = sampleTexture.LoadImage(byteArray);//..this will auto-resize the texture dimensions.

            if (isLoaded)
            {

                for (int i = 0; i < sampleTexture.width; i += tilingSize)
                {
                    for (int k = 0; k < sampleTexture.height; k += tilingSize)
                    {
                        if (sampleTexture.GetPixel(i + (tilingSize / 2), k + (tilingSize/2)).a < threshholdValue) continue;

                        Sprite tmpSprite = Sprite.Create(sampleTexture, new Rect(i, k, tilingSize, tilingSize), new Vector2(.5f, .5f), tilingSize, 1, SpriteMeshType.FullRect);


                        uiController.SetImportImagePreview(tmpSprite);



                        var tile = new Tile {sprite = tmpSprite};

                        tileList.Add(tile);
                    }
                 }
                print("list length: " + tileList.Count);

                tiles = tileList.ToArray();
                uiController.SetTilePaletteUI(tiles);
            }
            else
            {
                uiController.DisplayMessage("texture not loaded. set to wong tiles");
                print("STEP 2: texture not loaded. set to wong tiles");

                return null;
            }
  

        return tiles;
    }

}
