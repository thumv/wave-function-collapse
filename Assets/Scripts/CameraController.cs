﻿using UnityEngine.EventSystems;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Camera cam;
    private Vector3 dragOrigin;

    public GameObject uiBar;

    void Start()
    {
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        if (!EventSystem.current.IsPointerOverGameObject()) {
            if (Input.GetMouseButtonDown(0))
            {
                dragOrigin = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
                dragOrigin = cam.ScreenToWorldPoint(dragOrigin);
            }

            if (Input.GetMouseButton(0))
            {
                var currentPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
                currentPos = cam.ScreenToWorldPoint(currentPos);
                var movePos = dragOrigin - currentPos;

                if (Mathf.Abs(transform.position.x + movePos.x) < 50 && Mathf.Abs(transform.position.y + movePos.y) < 30)
                    transform.position = transform.position + movePos;
            }


            if (Mathf.Abs(Input.mouseScrollDelta.y) > 0.5f)
            {
                if (Input.mouseScrollDelta.y > 0 && cam.orthographicSize < 40)
                    cam.orthographicSize++;

                if (Input.mouseScrollDelta.y < 0 && cam.orthographicSize > 1)
                    cam.orthographicSize--;
            }
        }
    }
}
