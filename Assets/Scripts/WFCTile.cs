﻿using UnityEngine;
using System.Collections.Generic;

public class WFCTile : MonoBehaviour
{
    public int[] possible_tiles;
    public int tileValue;
    public bool isCollapsed;
    public bool hasBeenVisited;
    void Start()
    {
        isCollapsed = false;
        hasBeenVisited = false;
    }

    public void _WFCTile()
    {
        isCollapsed = false;
        possible_tiles = new int[8];

        for (int i = 0; i < possible_tiles.Length; i++)
        {
            possible_tiles[i] = 1;
        }
    }
    public WFCTile(int tileAmount)
    {
        isCollapsed = false;
        possible_tiles = new int[tileAmount];

        for (int i = 0; i < tileAmount; i++)
        {
            possible_tiles[i] = 1;
        }
    }
    public float GetEntropy()
    {
        if (isCollapsed) return 0;
        float value = 0;
        int flag = -1;

        for (int i = 0; i < possible_tiles.Length; i++)
        {
            flag = 0;
            value  += possible_tiles[i];
            if (possible_tiles[i] == 1) flag = i;
        }

        if (value == 1)
        {
            tileValue = flag;
        }
        else
        {
            tileValue = -1;
        }
        //Normierung auf 1
        if (possible_tiles.Length != 0)
            value = value/ possible_tiles.Length;

        return value;
    }

    internal void SetAllValuesZero(int currentIndex)
    {
        for (int i = 0; i < possible_tiles.Length; i++)
        {
            possible_tiles[i] = 0;
        }
    }

    internal void SetValue(int currentIndex, int value)
    {
        possible_tiles[currentIndex] = value;
    }

    internal int GetPossible()
    {

        List<int> canditates = new List<int>();

        for (int i = 0; i < possible_tiles.Length; i++)
        {
            if (possible_tiles[i] == 1)
            {
                canditates.Add(i);
            }
        }
        if (canditates.Count > 0)
        {

            int randomAmount = UnityEngine.Random.Range(0, canditates.Count);

            return canditates[randomAmount];

        }
        else
            return -1;  
    }

    internal void RemovePossibility(int currentIndex)
    {
        if (possible_tiles != null)
        {
            possible_tiles[currentIndex] = 0;
        }
    }

    internal void SetCollapsed(int index)
    {
        for (int i = 0; i < possible_tiles.Length; i++)
        {
            possible_tiles[i] = 0;
        }
        possible_tiles[index] = 1;
        tileValue = index;
        isCollapsed = true;
    }
}